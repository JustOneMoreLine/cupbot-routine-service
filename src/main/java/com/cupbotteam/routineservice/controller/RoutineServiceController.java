package com.cupbotteam.routineservice.controller;

import com.cupbotteam.routineservice.core.database.DailyTweet;
import com.cupbotteam.routineservice.repository.DailyTweetRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import java.util.List;

@Controller
public class RoutineServiceController {
    private final DailyTweetRepo dailyTweetRepo;

    @Autowired
    public RoutineServiceController(DailyTweetRepo dailyTweetRepo) {
        this.dailyTweetRepo = dailyTweetRepo;
    }

    @GetMapping("/")
    public String listLoggedTweets(Model model) {
        List<DailyTweet> loggedTweets = dailyTweetRepo.findAll();
        model.addAttribute("tweets", loggedTweets);
        return "home";
    }

    /**
     * An endpoint to ping the service and keep it alive,
     * may also be use to trigger a function that required to run
     * daily.
     *
     * @return ResponseEntity with 200 HTTP Status
     */
    @GetMapping("/wakeMe")
    public ResponseEntity wakeMe() {
        return new ResponseEntity(HttpStatus.OK);
    }
}
