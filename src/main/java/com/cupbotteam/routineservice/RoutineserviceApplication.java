package com.cupbotteam.routineservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@EnableEurekaClient
@EnableScheduling
@RestController
@EnableJpaRepositories
public class RoutineserviceApplication {

    public static void main(String[] args) {
        SpringApplication.run(RoutineserviceApplication.class, args);
    }

    @RequestMapping("/")
    public String home() {
        return "Eureka Client application";
    }
}
