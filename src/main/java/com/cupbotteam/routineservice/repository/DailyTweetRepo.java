package com.cupbotteam.routineservice.repository;

import com.cupbotteam.routineservice.core.database.DailyTweet;
import org.springframework.data.jpa.repository.JpaRepository;
import java.time.LocalDate;
import java.util.List;


public interface DailyTweetRepo extends JpaRepository<DailyTweet, Long> {
    List<DailyTweet> findAllByTweetDate(LocalDate publicationDate);

    List<DailyTweet> findAllByTweetDateBetween(
            LocalDate publicationTimeStart,
            LocalDate publicationTimeEnd);
}
