package com.cupbotteam.routineservice.core;

/**
 * A java class use to set a JSON structure for sending HTTP request,
 * especially with database.
 * This would hold a cafe information.
 */
public class Cafe {
    private CafeId id;
    private CafeAddress cafeAddress;
    private long cafeOfTheWeekWins = 0;
    private long cafeOfAllTimeRank = Long.MAX_VALUE;

    public Cafe() {}

    public Cafe(CafeId id, CafeAddress cafeAddress) {
        this.id = id;
        this.cafeAddress = cafeAddress;
        this.cafeOfTheWeekWins = 0;
        this.cafeOfAllTimeRank = Long.MAX_VALUE;
    }

    public void setId(CafeId id) {
        this.id = id;
    }

    public CafeId getId() {
        return id;
    }

    public void setCafeAddress(CafeAddress cafeAddress) {
        this.cafeAddress = cafeAddress;
    }

    public CafeAddress getCafeAddress() {
        return cafeAddress;
    }

    public void setCafeOfTheWeekWins(long cafeOfTheWeekWins) {
        this.cafeOfTheWeekWins = cafeOfTheWeekWins;
    }

    public long getCafeOfTheWeekWins() {
        return cafeOfTheWeekWins;
    }

    public void setCafeOfAllTimeRank(long cafeOfAllTimeRank) {
        this.cafeOfAllTimeRank = cafeOfAllTimeRank;
    }

    public long getCafeOfAllTimeRank() {
        return cafeOfAllTimeRank;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }

        if (!(obj instanceof Cafe)) {
            return false;
        }

        Cafe obj1 = (Cafe) obj;
        if (this.id.equals(obj1.id) && this.cafeAddress.equals(obj1.cafeAddress)) {
            return true;
        }

        return false;
    }
}
