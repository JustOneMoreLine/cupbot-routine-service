package com.cupbotteam.routineservice.core;

/**
 * A java class use to set a JSON structure for sending HTTP request,
 * especially for database.
 * This would hold cafe owner's twitter id and cafe's name,
 * its an composite value and a primary key for Cafe.
 */
public class CafeId {
    private long cafeOwnerTwitterId;
    private String cafeName;

    public CafeId() {}

    public CafeId(long cafeOwnerTwitterId, String cafeName) {
        this.cafeOwnerTwitterId = cafeOwnerTwitterId;
        this.cafeName = cafeName;
    }

    public void setCafeOwnerTwitterId(long cafeOwnerTwitterId) {
        this.cafeOwnerTwitterId = cafeOwnerTwitterId;
    }

    public long getCafeOwnerTwitterId() {
        return cafeOwnerTwitterId;
    }

    public void setCafeName(String cafeName) {
        this.cafeName = cafeName;
    }

    public String getCafeName() {
        return cafeName;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof CafeId)) {
            return false;
        }

        CafeId obj1 = (CafeId) obj;
        if (this.cafeName.equals(obj1.cafeName)
                && this.cafeOwnerTwitterId == obj1.cafeOwnerTwitterId) {
            return true;
        }
        return false;
    }
}
