package com.cupbotteam.routineservice.core;

/**
 * A java class use to set a JSON structure for sending HTTP request
 * only for authentication.
 * This would hold a token's challange.
 */
public class CrcToken {
    private String token;

    public void setToken(String token) {
        this.token = token;
    }

    public String getToken() {
        return this.token;
    }

    public String toString() {
        return "Token: " + this.token;
    }
}
