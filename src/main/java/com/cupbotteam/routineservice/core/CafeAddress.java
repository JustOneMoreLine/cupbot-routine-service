package com.cupbotteam.routineservice.core;

/**
 * A java class use to set a JSON structure for sending HTTP request,
 * especially for database.
 * This would hold a cafe's address, its an composite value for Cafe.
 */
public class CafeAddress {
    private String street;
    private String subDistrict; // kelurahan
    private String district; // kecamatan
    private String city; // test
    private String province;

    public CafeAddress() {}

    public CafeAddress(String street,
                       String subDistrict,
                       String district,
                       String city,
                       String province) {
        this.street = street;
        this.subDistrict = subDistrict;
        this.district = district;
        this.city = city;
        this.province = province;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getStreet() {
        return street;
    }

    public void setSubDistrict(String subDistrict) {
        this.subDistrict = subDistrict;
    }

    public String getSubDistrict() {
        return subDistrict;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getDistrict() {
        return district;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCity() {
        return city;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getProvince() {
        return province;
    }

    @Override
    public String toString() {
        return street + ", " + subDistrict + ", " + district + ", " + city + ", " + province;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }

        if (!(obj instanceof CafeAddress)) {
            return false;
        }

        CafeAddress obj1 = (CafeAddress) obj;
        if (this.street.equals(obj1.street)
                && this.subDistrict.equals(obj1.subDistrict)
                && this.district.equals(obj1.district)
                && this.city.equals(obj1.city)
                && this.province.equals(obj1.province)) {
            return true;
        }
        return false;
    }
}
