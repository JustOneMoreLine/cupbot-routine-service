package com.cupbotteam.routineservice.core;

import java.time.LocalDateTime;

/**
 * A java class use to set a JSON structure for sending HTTP request,
 * especially for database.
 * This would hold information about a cafe's promo.
 */
public class CafePromo {
    private long id;
    private CafeId cafeId;
    private String promoCode;
    private LocalDateTime startOfPromo;
    private LocalDateTime endOfPromo;
    private String promoDescription;

    public CafePromo() {}

    public CafePromo(
            long id,
            CafeId cafeId,
            String promoCode,
            LocalDateTime startOfPromo,
            LocalDateTime endOfPromo,
            String promoDescription) {
        this.id = id;
        this.cafeId = cafeId;
        this.promoCode = promoCode;
        this.startOfPromo = startOfPromo;
        this.endOfPromo = endOfPromo;
        this.promoDescription = promoDescription;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setCafeId(CafeId cafeId) {
        this.cafeId = cafeId;
    }

    public CafeId getCafeId() {
        return cafeId;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setStartOfPromo(LocalDateTime startOfPromo) {
        this.startOfPromo = startOfPromo;
    }

    public LocalDateTime getStartOfPromo() {
        return startOfPromo;
    }

    public void setEndOfPromo(LocalDateTime endOfPromo) {
        this.endOfPromo = endOfPromo;
    }

    public LocalDateTime getEndOfPromo() {
        return endOfPromo;
    }

    public void setPromoDescription(String promoDescription) {
        this.promoDescription = promoDescription;
    }

    public String getPromoDescription() {
        return promoDescription;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof CafePromo)) {
            return false;
        }

        CafePromo obj1 = (CafePromo) obj;
        if (this.id == obj1.id
                && this.cafeId.equals(obj1.cafeId)
                && this.promoCode.equals(obj1.promoCode)
                && this.startOfPromo.equals(obj1.startOfPromo)
                && this.endOfPromo.equals(obj1.endOfPromo)
                && this.promoDescription.equals(obj1.promoDescription)) {
            return true;
        }
        return false;
    }
}
