package com.cupbotteam.routineservice.core.database;

import lombok.NoArgsConstructor;
import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@NoArgsConstructor
@Table(name = "dailyTweet")
@Entity
public class DailyTweet {
    @Id
    long tweetId;
    @Column
    String cafeName;
    @Column
    long cafeOwnerTwitterId;
    @Column
    LocalDate tweetDate;
    final String zoneId = "GMT+7";

    public DailyTweet(long tweetId, String cafeName, long cafeOwnerTwitterId) {
        this.tweetId = tweetId;
        this.cafeName = cafeName;
        this.cafeOwnerTwitterId = cafeOwnerTwitterId;
        this.tweetDate = LocalDate.now();
    }

    public void setTweetId(long tweetId) {
        this.tweetId = tweetId;
    }

    public long getTweetId() {
        return tweetId;
    }

    public void setCafeName(String cafeName) {
        this.cafeName = cafeName;
    }

    public String getCafeName() {
        return cafeName;
    }

    public void setCafeOwnerTwitterId(long cafeOwnerTwitterId) {
        this.cafeOwnerTwitterId = cafeOwnerTwitterId;
    }

    public long getCafeOwnerTwitterId() {
        return cafeOwnerTwitterId;
    }

    public void setTweetDate(LocalDate tweetDate) {
        this.tweetDate = tweetDate;
    }

    public LocalDate getTweetDate() {
        return tweetDate;
    }

    public String getZoneId() {
        return zoneId;
    }
}
