package com.cupbotteam.routineservice.utility;

import org.apache.tomcat.util.buf.HexUtils;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/**
 * A class use to hash a string by HmacSHA256, use for challange-response check
 * authentication.
 */
public class Sha256 {
    private static final String key = "I jump over 31 foxes and 90 of them bite my leg";
    public static String hash(String code) {
        try {
            Mac sha256Mac = Mac.getInstance("HmacSHA256");
            SecretKeySpec secretKeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "HmacSHA256");
            sha256Mac.init(secretKeySpec);

            return HexUtils.toHexString(sha256Mac.doFinal(code.getBytes("UTF-8")));
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
    }
}
