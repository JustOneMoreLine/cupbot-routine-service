package com.cupbotteam.routineservice.utility;

import com.cupbotteam.routineservice.core.CrcToken;
import org.springframework.web.client.RestTemplate;

/**
 * A class use to handle authentication of this service's HTTP request to other services.
 */
public class ResponseTokenGenerator {
    protected static RestTemplate ask;

    public static void setAsk(RestTemplate ask) {
        ResponseTokenGenerator.ask = ask;
    }

    /**
     * Get's a service's challange token from it hostname, and creates a
     * response token to be use in a HTTP request to that service.
     *
     * @param hostname , a string that contains service's hostname.
     * @return a string of response token.
     */
    public static String generate(String hostname) {
        String challangeUri = "https://" + hostname + "/challange";
        if (ResponseTokenGenerator.ask == null) {
            ResponseTokenGenerator.setAsk(new RestTemplate());
        }
        CrcToken crcToken = ask.getForObject(challangeUri, CrcToken.class);
        if (crcToken == null) {
            return null;
        }
        String response = Sha256.hash(crcToken.getToken());
        return response;
    }
}

