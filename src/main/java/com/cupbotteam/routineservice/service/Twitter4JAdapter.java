package com.cupbotteam.routineservice.service;

import twitter4j.DirectMessage;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.RateLimitStatus;
import twitter4j.Status;
import twitter4j.StatusUpdate;
import java.util.Map;

/**
 * An interface for Twitter4JAdapter,
 * use as an adapter between services and Twitter4J.
 */
public interface Twitter4JAdapter {

    public Status createFavorite(long id);

    public Status destroyFavorite(long id);

    public Status retweetStatus(long statusId);

    public Status unRetweetStatus(long statusId);

    public Status showStatus(long id);

    public Status destroyStatus(long id);

    public Status updateStatus(String status);

    public Status updateStatus(StatusUpdate statusUpdate);

    public DirectMessage showDirectMessage(long id);

    public DirectMessage destroyDirectMessage(long id);

    public DirectMessage sendDirectMessage(long recipientId, String text);

    public Map<String, RateLimitStatus> getRateLimitStatus()
            ;

    public QueryResult search(Query query);
}
