package com.cupbotteam.routineservice.service;

import org.springframework.stereotype.Component;
import twitter4j.AsyncTwitter;
import twitter4j.AsyncTwitterFactory;
import twitter4j.DirectMessage;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.RateLimitStatus;
import twitter4j.Status;
import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;
import java.util.Map;

/**
 * An implementation of Twitter4JAdapter,
 * use as an adapter between service and Twitter4J.
 */
@Component
public class Twitter4JAdapterImpl implements Twitter4JAdapter {
    Twitter twitter;
    AsyncTwitter asyncTwitter;

    public Twitter4JAdapterImpl() {
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true)
                .setOAuthConsumerKey("aiOQsUNsgK9EeueMoLtH0GWLO")
                .setOAuthConsumerSecret("p3Z6ds2jsDRyBeieHJBCjLKk791cthDJxoVS6OO39A34Yq9Hlj")
                .setOAuthAccessToken("1247058788189782016-zz9D9lnRtnMKve6KhQ6GSR8NhX7OMc")
                .setOAuthAccessTokenSecret("6CQFLn46VCvGdcV4mjkEtov47lk76G8MQPUwBPU93wJnb");
        Configuration con = cb.build();
        TwitterFactory tf = new TwitterFactory(con);
        AsyncTwitterFactory asyncTf = new AsyncTwitterFactory(con);
        Twitter twitter = tf.getInstance();
        AsyncTwitter asyncTwitter = asyncTf.getInstance();
        this.twitter = twitter;
        this.asyncTwitter = asyncTwitter;
    }

    public Twitter4JAdapterImpl(Twitter twitter, AsyncTwitter asyncTwitter) {
        this.twitter = twitter;
        this.asyncTwitter = asyncTwitter;
    }

    @Override
    public Status destroyFavorite(long id) {
        try {
            return twitter.destroyFavorite(id);
        } catch (TwitterException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public Status unRetweetStatus(long statusId) {
        try {
            return twitter.unRetweetStatus(statusId);
        } catch (TwitterException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public Status createFavorite(long id) {
        try {
            return twitter.createFavorite(id);
        } catch (TwitterException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public Status retweetStatus(long statusId) {
        try {
            return twitter.retweetStatus(statusId);
        } catch (TwitterException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public Status showStatus(long id) {
        try {
            return twitter.showStatus(id);
        } catch (TwitterException e) {
            System.out.println(e);
        }
        return null;
    }


    @Override
    public Status destroyStatus(long id) {
        try {
            return twitter.destroyStatus(id);
        } catch (TwitterException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public Status updateStatus(String status) {
        try {
            return twitter.updateStatus(status);
        } catch (TwitterException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public Status updateStatus(StatusUpdate statusUpdate) {
        try {
            return twitter.updateStatus(statusUpdate);
        } catch (TwitterException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public DirectMessage showDirectMessage(long id) {
        try {
            return twitter.showDirectMessage(id);
        } catch (TwitterException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public DirectMessage destroyDirectMessage(long id) {
        try {
            return twitter.destroyDirectMessage(id);
        } catch (TwitterException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public DirectMessage sendDirectMessage(long recipientId, String text) {
        try {
            return twitter.sendDirectMessage(recipientId, text);
        } catch (TwitterException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public Map<String, RateLimitStatus> getRateLimitStatus() {
        try {
            return twitter.getRateLimitStatus();
        } catch (TwitterException e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public QueryResult search(Query query) {
        try {
            return twitter.search(query);
        } catch (TwitterException e) {
            System.out.println(e);
        }
        return null;
    }
}

