package com.cupbotteam.routineservice.service;

import com.cupbotteam.routineservice.core.Cafe;
import com.cupbotteam.routineservice.core.CafeId;
import com.cupbotteam.routineservice.utility.ResponseTokenGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;
import java.util.List;

@Service
/**
 * An implementation of CafeDatabaseAdapter,
 * use to use to interact with database service,
 * particularly the Cafe table.
 */
public class CafeDatabaseAdapterImpl implements CafeDatabaseAdapter {
    DiscoveryClient discoveryClient;
    String endpoint;
    private final WebClient client;
    private RestTemplate restTemplate;

    @Autowired
    public CafeDatabaseAdapterImpl(DiscoveryClient discoveryClient) {
        this.discoveryClient = discoveryClient;
        this.endpoint = "https://" + getHost() + "/cafe";
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(endpoint);
        this.client = WebClient.builder()
                .baseUrl(builder.toUriString())
                .build();
    }

    public CafeDatabaseAdapterImpl(DiscoveryClient discoveryClient, WebClient client, RestTemplate restTemplate) {
        this.discoveryClient = discoveryClient;
        this.endpoint = "https://" + getHost() + "/cafe";
        this.client = client;
        this.restTemplate = restTemplate;
        ResponseTokenGenerator.setAsk(restTemplate);
    }

    /**
     * Use to get a registered cafe based on the requested cafeId.
     *
     * @param cafeId , the requested Cafe's id.
     * @return A list that contains one cafe, if the id doesn't exist
     * returns an empty list.
     */
    @Override
    public Cafe[] getCafe(CafeId cafeId) {
        String response = ResponseTokenGenerator.generate(getHost());
        Mono<Cafe[]> result = client.post()
                .uri("/getCafe?response=" + response)
                .accept(MediaType.APPLICATION_JSON)
                .bodyValue(cafeId)
                .retrieve()
                .bodyToMono(Cafe[].class);
        return result.block();
    }

    /**
     * Use to get a registered cafe based on the requested cafe owner's Twitter id.
     *
     * @param cafeOwnerTwitterId , the requested Cafe owner's id.
     * @return A list that contains one or more cafe, if the id doesn't exist
     * returns an empty list.
     */
    @Override
    public Cafe[] getCafeByOwnerTwitterId(long cafeOwnerTwitterId) {
        String response = ResponseTokenGenerator.generate(getHost());
        Mono<Cafe[]> result = client.post()
                .uri("/getCafeByOwnerTwitterId?response=" + response)
                .accept(MediaType.APPLICATION_JSON)
                .bodyValue(cafeOwnerTwitterId)
                .retrieve()
                .bodyToMono(Cafe[].class);
        return result.block();
    }

    /**
     * Use to get all registered cafe.
     *
     * @return A list of cafe.
     */
    @Override
    public Cafe[] getAllCafe() {
        String response = ResponseTokenGenerator.generate(getHost());
        Mono<Cafe[]> result = client.get()
                .uri("/getAllCafe?response=" + response)
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(Cafe[].class);
        return result.block();
    }

    /**
     * Add a new cafe to the database.
     *
     * @param cafe , the new registered cafe.
     * @return the cafe saved by database.
     */
    @Override
    public Cafe addCafe(Cafe cafe) {
        String response = ResponseTokenGenerator.generate(getHost());
        Mono<Cafe> result = client.post()
                .uri("/addCafe?response=" + response)
                .accept(MediaType.APPLICATION_JSON)
                .bodyValue(cafe)
                .retrieve()
                .bodyToMono(Cafe.class);
        return result.block();
    }

    /**
     * Adds a list of new cafes.
     *
     * @param listOfCafe , list of cafes.
     * @return list of cafes saved by database.
     */
    @Override
    public Cafe[] addMultipleCafe(List<Cafe> listOfCafe) {
        String response = ResponseTokenGenerator.generate(getHost());
        Mono<Cafe[]> result = client.post()
                .uri("/addMultipleCafe?response=" + response)
                .accept(MediaType.APPLICATION_JSON)
                .bodyValue(listOfCafe)
                .retrieve()
                .bodyToMono(Cafe[].class);
        return result.block();
    }

    /**
     * Deletes a cafe.
     *
     * @param cafeId , cafe's id to be deleted.
     * @return a list that contains one cafe that have just been deleted,
     * if there are no known cafe with that cafe id, returns an empty list.
     */
    @Override
    public Cafe[] deleteCafe(CafeId cafeId) {
        String response = ResponseTokenGenerator.generate(getHost());
        Mono<Cafe[]> result = client.post()
                .uri("/deleteCafe?response=" + response)
                .accept(MediaType.APPLICATION_JSON)
                .bodyValue(cafeId)
                .retrieve()
                .bodyToMono(Cafe[].class);
        return result.block();
    }

    /**
     * Delete a list of cafes.
     *
     * @param idList , list of cafes to be deleted.
     * @return a list deleted cafes.
     */
    @Override
    public Cafe[] deleteMultipleCafe(List<CafeId> idList) {
        String response = ResponseTokenGenerator.generate(getHost());
        Mono<Cafe[]> result = client.post()
                .uri("/deleteMultipleCafe?response=" + response)
                .accept(MediaType.APPLICATION_JSON)
                .bodyValue(idList)
                .retrieve()
                .bodyToMono(Cafe[].class);
        return result.block();
    }

    /**
     * Delete all registered cafes.
     *
     * @return a list of deleted cafes.
     */
    @Override
    public Cafe[] deleteAllCafe() {
        String response = ResponseTokenGenerator.generate(getHost());
        Mono<Cafe[]> result = client.get()
                .uri("/deleteAllCafe?response=" + response)
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(Cafe[].class);
        return result.block();
    }

    /**
     * Use to increment cafeOfTheWeekWins based on the requested cafeId.
     *
     * @param cafeId , the requested Cafe's id.
     * @return A list that contains one cafe, if the id doesn't exist
     * returns an empty list.
     */
    @Override
    public Cafe[] incrementCotwWins(CafeId cafeId) {
        String response = ResponseTokenGenerator.generate(getHost());
        Mono<Cafe[]> result = client.post()
                .uri("/incrementCotwWins?response=" + response)
                .accept(MediaType.APPLICATION_JSON)
                .bodyValue(cafeId)
                .retrieve()
                .bodyToMono(Cafe[].class);
        return result.block();
    }

    /**
     * Checks database's hostname from service registry.
     *
     * @return a string of database's hostname, null if database is not available.
     */
    private String getHost() {
        List<String> availableService = discoveryClient.getServices();
        if (availableService.contains("database")) {
            String host = discoveryClient.getInstances("database").get(0).getUri().getHost();
            System.out.println("DATABASE REACHED AT: " + host);
            return host;
        } else {
            System.out.println("DATABASE CANNOT BE REACHED");
            try {
                Thread.sleep(5000);
            } catch (Exception e) {
                System.out.println(e);
            }
            return getHost();
        }
    }
}
