package com.cupbotteam.routineservice.service;

import com.cupbotteam.routineservice.core.CafeId;
import com.cupbotteam.routineservice.core.CafePromo;
import com.cupbotteam.routineservice.utility.ResponseTokenGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;
import java.util.List;

@Service
/**
 * An implementation of PromoDatabaseAdapter,
 * use to interact with database service,
 * particularly CafePromo table.
 */
public class PromoDatabaseAdapterImpl implements PromoDatabaseAdapter {
    DiscoveryClient discoveryClient;
    String endpoint;
    private final WebClient client;
    private RestTemplate restTemplate;

    @Autowired
    public PromoDatabaseAdapterImpl(DiscoveryClient discoveryClient) {
        this.discoveryClient = discoveryClient;
        this.endpoint = "https://" + getHost() + "/promo";
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(endpoint);
        client = WebClient.builder()
                .baseUrl(builder.toUriString())
                .build();
    }

    public PromoDatabaseAdapterImpl(DiscoveryClient discoveryClient, WebClient client, RestTemplate restTemplate) {
        this.discoveryClient = discoveryClient;
        this.endpoint = "https://" + getHost() + "/cafe";
        this.client = client;
        this.restTemplate = restTemplate;
        ResponseTokenGenerator.setAsk(restTemplate);
    }

    /**
     * Gets a promo by it's id.
     *
     * @param id , promo's id.
     * @return a list that contains one promo that's requested,
     * if no known promo with that id is found, return an empty
     * list.
     */
    @Override
    public CafePromo[] getPromoById(long id) {
        String response = ResponseTokenGenerator.generate(getHost());
        Mono<CafePromo[]> result = client.post()
                .uri("/getPromoById?response=" + response)
                .accept(MediaType.APPLICATION_JSON)
                .bodyValue(id)
                .retrieve()
                .bodyToMono(CafePromo[].class);
        return result.block();
    }

    /**
     * Gets a list of promo from that particular Cafe by
     * the cafe's id.
     *
     * @param cafeId , the cafe's id which the promo belongs to.
     * @return A list of CafePromo of requested cafe.
     */
    @Override
    public CafePromo[] getCafePromo(CafeId cafeId) {
        String response = ResponseTokenGenerator.generate(getHost());
        Mono<CafePromo[]> result = client.post()
                .uri("/getCafePromo?response=" + response)
                .accept(MediaType.APPLICATION_JSON)
                .bodyValue(cafeId)
                .retrieve()
                .bodyToMono(CafePromo[].class);
        return result.block();
    }

    /**
     * Gets all registered promos.
     *
     * @return A list of CafePromo.
     */
    @Override
    public CafePromo[] getAllPromo() {
        String response = ResponseTokenGenerator.generate(getHost());
        Mono<CafePromo[]> result = client.get()
                .uri("/getAllPromo?response=" + response)
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(CafePromo[].class);
        return result.block();
    }

    /**
     * Adds a new promo to database.
     *
     * @param cafePromo , the cafe promo to be registerd.
     * @return a cafe promo that have just saved to database.
     */
    @Override
    public CafePromo addPromo(CafePromo cafePromo) {
        String response = ResponseTokenGenerator.generate(getHost());
        Mono<CafePromo> result = client.post()
                .uri("/addPromo?response=" + response)
                .accept(MediaType.APPLICATION_JSON)
                .bodyValue(cafePromo)
                .retrieve()
                .bodyToMono(CafePromo.class);
        return result.block();
    }

    /**
     * Adds a list of promo.
     *
     * @param cafePromoList , a list of cafe promos.
     * @return a list of cafe promo that have just saved to database.
     */
    @Override
    public CafePromo[] addMultiplePromo(List<CafePromo> cafePromoList) {
        String response = ResponseTokenGenerator.generate(getHost());
        Mono<CafePromo[]> result = client.post()
                .uri("/addMultiplePromo?response=" + response)
                .accept(MediaType.APPLICATION_JSON)
                .bodyValue(cafePromoList)
                .retrieve()
                .bodyToMono(CafePromo[].class);
        return result.block();
    }

    /**
     * Deletes a promo by it's id.
     *
     * @param id , id of to be deleted promo.
     * @return a promo that have just been deleted from database.
     */
    @Override
    public CafePromo[] deletePromoById(long id) {
        String response = ResponseTokenGenerator.generate(getHost());
        Mono<CafePromo[]> result = client.post()
                .uri("/deletePromoById?response=" + response)
                .accept(MediaType.APPLICATION_JSON)
                .bodyValue(id)
                .retrieve()
                .bodyToMono(CafePromo[].class);
        return result.block();
    }

    /**
     * Deletes all promo held by that cafe.
     *
     * @param cafeId , cafe's id of to be deleted promos belong to.
     * @return a list of promo that have just been deleted.
     */
    @Override
    public CafePromo[] deleteCafePromo(CafeId cafeId) {
        String response = ResponseTokenGenerator.generate(getHost());
        Mono<CafePromo[]> result = client.post()
                .uri("/deleteCafePromo?response=" + response)
                .accept(MediaType.APPLICATION_JSON)
                .bodyValue(cafeId)
                .retrieve()
                .bodyToMono(CafePromo[].class);
        return result.block();
    }

    /**
     * Deletes a list of promo by ids.
     *
     * @param ids , list of promo's id to be deleted.
     * @return a list of promo that have just been deleted.
     */
    @Override
    public CafePromo[] deleteMultiplePromoById(List<Long> ids) {
        String response = ResponseTokenGenerator.generate(getHost());
        Mono<CafePromo[]> result = client.post()
                .uri("/deleteMultiplePromoById?response=" + response)
                .accept(MediaType.APPLICATION_JSON)
                .bodyValue(ids)
                .retrieve()
                .bodyToMono(CafePromo[].class);
        return result.block();
    }

    /**
     * Deletes all promo that belongs to a list of cafe by it's id.
     *
     * @param idList , a list of cafe's id which to be deleted promos belong to.
     * @return a list of promo that have just been deleted.
     */
    @Override
    public CafePromo[] deleteMultipleCafePromo(List<CafeId> idList) {
        String response = ResponseTokenGenerator.generate(getHost());
        Mono<CafePromo[]> result = client.post()
                .uri("/deleteMultipleCafePromo?response=" + response)
                .accept(MediaType.APPLICATION_JSON)
                .bodyValue(idList)
                .retrieve()
                .bodyToMono(CafePromo[].class);
        return result.block();
    }

    /**
     * Deletes all registered promo.
     *
     * @return a list of promo that have just been deleted.
     */
    @Override
    public CafePromo[] deleteAllPromo() {
        String response = ResponseTokenGenerator.generate(getHost());
        Mono<CafePromo[]> result = client.get()
                .uri("/deleteAllPromo?response=" + response)
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(CafePromo[].class);
        return result.block();
    }

    /**
     * Checks database's hostname from service registry.
     *
     * @return a string of database's hostname, null if database is not available.
     */
    private String getHost() {
        List<String> availableService = discoveryClient.getServices();
        if (availableService.contains("database")) {
            String host = discoveryClient.getInstances("database").get(0).getUri().getHost();
            System.out.println("DATABASE REACHED AT: " + host);
            return host;
        } else {
            System.out.println("DATABASE CANNOT BE REACHED");
            try {
                Thread.sleep(5000);
            } catch (Exception e) {
                System.out.println(e);
            }
            return getHost();
        }
    }
}
