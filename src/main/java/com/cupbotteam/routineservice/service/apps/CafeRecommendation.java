package com.cupbotteam.routineservice.service.apps;

import com.cupbotteam.routineservice.core.Cafe;
import com.cupbotteam.routineservice.core.database.DailyTweet;
import com.cupbotteam.routineservice.repository.DailyTweetRepo;
import com.cupbotteam.routineservice.service.CafeDatabaseAdapter;
import com.cupbotteam.routineservice.service.Twitter4JAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import twitter4j.Status;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Component
public class CafeRecommendation {
    private CafeDatabaseAdapter cafeDatabaseAdapter;
    private Twitter4JAdapter twitter;
    private ArrayList<String> salt;
    private WebClient webClient;
    private final ArrayList<Cafe> dailyRecommendedCafe;
    private final DailyTweetRepo dailyTweetRepo;

    @Autowired
    public CafeRecommendation(CafeDatabaseAdapter cafeDatabaseAdapter,
                              Twitter4JAdapter twitter,
                              DailyTweetRepo dailyTweetRepo) {
        this.cafeDatabaseAdapter = cafeDatabaseAdapter;
        this.twitter = twitter;
        this.dailyTweetRepo = dailyTweetRepo;
        this.salt = new ArrayList<>();
        salt.add("Check it out!");
        salt.add("More expresso less depresso");
        salt.add("Coffee comes with its cup!");
        this.dailyRecommendedCafe = new ArrayList<>();
    }

    @Scheduled(cron = "0 0/30 8-23 * * *", zone = "GMT+7")
    public void dailyTweet() {
        System.out.println("Daily Tweet Triggered");
        Cafe recommendedCafe = getRecommendedCafe();
        if (recommendedCafe == null) {
            // do nothing
        } else {
            String cafeName = recommendedCafe.getId().getCafeName();
            long cafeOwnerTwitterId = recommendedCafe.getId().getCafeOwnerTwitterId();
            String cafeAddress = recommendedCafe.getCafeAddress().toString();
            String text = String.format(
                    "[DAILY RECOMMENDS]\n" +
                        "Cafe: " + cafeName + "\n" +
                        "Alamat: " + cafeAddress + "\n" +
                        salt.get((int) Math.floor(Math.random() * 2))
            );
            Status newTweet = twitter.updateStatus(text);
            DailyTweet logTweet = new DailyTweet(
                    newTweet.getId(),
                    cafeName,
                    cafeOwnerTwitterId);
            dailyTweetRepo.saveAndFlush(logTweet);
        }
    }

    @Scheduled(cron = "0 0 0 * * *", zone = "GMT+7")
    public void dailyCleanUp() {
        dailyRecommendedCafe.clear();
    }

    @Scheduled(cron = "0 0 0 * * SUN", zone = "GMT+7")
    public void rankUpdates() {
        Cafe[] allKnownCafeRaw = cafeDatabaseAdapter.getAllCafe();
        List<Cafe> allKnownCafe = cafeArrToList(allKnownCafeRaw);
        allKnownCafe.sort(new SortRankByCafeWins());
        List<Cafe> returnedUpdatedCafeList = updateRankOnEachCafe(allKnownCafe);

        Cafe[] currentCafeInDatabase = cafeDatabaseAdapter.getAllCafe();
        if (currentCafeInDatabase.length == returnedUpdatedCafeList.size()) {
            currentCafeInDatabase = cafeDatabaseAdapter.deleteAllCafe();
            if (currentCafeInDatabase.length == returnedUpdatedCafeList.size()) {
                cafeDatabaseAdapter.addMultipleCafe(returnedUpdatedCafeList);
            } else {
                cafeDatabaseAdapter.addMultipleCafe(sendBackToDb(currentCafeInDatabase));
                rankUpdates();
            }
        } else {
            cafeDatabaseAdapter.addMultipleCafe(sendBackToDb(currentCafeInDatabase));
            rankUpdates();
        }
    }

    private List<Cafe> updateRankOnEachCafe(List<Cafe> cafeList) {
        List<Cafe> returnedCafeList = new ArrayList<>();
        long currRank = 0;
        for (int i = 0; i < cafeList.size(); i++) {
            Cafe currCafe = cafeList.get(i);
            currCafe.setCafeOfAllTimeRank(++currRank);
            returnedCafeList.add(currCafe);
        }
        return returnedCafeList;
    }

    private List<Cafe> cafeArrToList(Cafe[] cafeList) {
        List<Cafe> cafes = new ArrayList<>();
        for (Cafe x : cafeList) {
            cafes.add(x);
        }
        return cafes;
    }

    private List<Cafe> sendBackToDb(Cafe[] cafeList) {
        List<Cafe> sendBack = new ArrayList<>();
        for (Cafe x : cafeList) {
            sendBack.add(x);
        }
        return sendBack;
    }

    private Cafe getRecommendedCafe() {
        List<Cafe> allKnownCafe = new ArrayList<>();
        Cafe[] allKnownCafeRaw = cafeDatabaseAdapter.getAllCafe();
        for (Cafe x : allKnownCafeRaw) {
            allKnownCafe.add(x);
        }
        allKnownCafe.sort(new SortRankByCafeWins());
        if (allKnownCafe.size() == 0) {
            return null;
        }
        return pickLuckyCafe(allKnownCafe);
    }

    private Cafe pickLuckyCafe(List<Cafe> cafeList) {
        if (cafeList.size() == 0) {
            return null;
        }
        List<Cafe> listOfLowestCafes = getListOfLowestCafes(cafeList);
        int luckyCafeIndex = (int) Math.floor(Math.random() * (listOfLowestCafes.size() - 1));
        Cafe luckyCafe = listOfLowestCafes.get(luckyCafeIndex);
        if (dailyRecommendedCafe.contains(luckyCafe)) {
            cafeList.remove(luckyCafe);
            return pickLuckyCafe(cafeList);
        }
        dailyRecommendedCafe.add(luckyCafe);
        return luckyCafe;
    }

    private List<Cafe> getListOfLowestCafes(List<Cafe> cafeList) {
        int lowestCafeIndex = cafeList.size() - 1;
        long lowestWin = cafeList.get(lowestCafeIndex).getCafeOfTheWeekWins();
        List<Cafe> listOfLowestCafes = new ArrayList<>();
        for (int i = lowestCafeIndex; i >= 0; i--) {
            if (cafeList.get(i).getCafeOfTheWeekWins() > lowestWin) {
                break;
            }
            listOfLowestCafes.add(cafeList.get(i));
        }
        return listOfLowestCafes;
    }



    class SortRankByCafeWins implements Comparator<Cafe> {
        @Override
        public int compare(Cafe o1, Cafe o2) {

            return (int) (o2.getCafeOfTheWeekWins() - o1.getCafeOfTheWeekWins());
        }
    }
}
