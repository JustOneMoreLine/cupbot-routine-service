package com.cupbotteam.routineservice.service.apps;

import com.cupbotteam.routineservice.core.Cafe;
import com.cupbotteam.routineservice.core.CafeId;
import com.cupbotteam.routineservice.core.database.DailyTweet;
import com.cupbotteam.routineservice.repository.DailyTweetRepo;
import com.cupbotteam.routineservice.service.CafeDatabaseAdapter;
import com.cupbotteam.routineservice.service.Twitter4JAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Component
public class CafeOfTheWeek {
    Status cafeOfTheWeekNominationsTweet;
    Status cafeOfTheWeekTweet;

    List<String> cafeOfTheWeekNominations = new ArrayList<>();
    HashMap<Integer, Integer> cafeOfTheWeekPollCount;
    HashMap<Integer, String> cafeOfTheWeekNominationsData;

    private static final String DATE_FORMAT = "yyyy-MM-dd";
    DateTimeFormatter format = DateTimeFormatter.ofPattern(DATE_FORMAT);

    private CafeDatabaseAdapter cafeDatabaseAdapter;
    private Twitter4JAdapter twitter;
    private final DailyTweetRepo dailyTweetRepo;

    @Autowired
    public CafeOfTheWeek(
                         Twitter4JAdapter twitter,
                         CafeDatabaseAdapter cafeDatabaseAdapter,
                         DailyTweetRepo dailyTweetRepo) {
        this.cafeDatabaseAdapter = cafeDatabaseAdapter;
        this.twitter = twitter;
        this.dailyTweetRepo = dailyTweetRepo;
    }

    @Scheduled(cron = "0 0 7 ? * *", zone = "GMT+7")
    public void selectCafeOfTheWeekNomination() {
        List<DailyTweet> latestCafeRecommendationTweets = getLatestCafeRecommendationTweets();
        String cafeOfTheDay = getCafeOfTheDay(latestCafeRecommendationTweets);
        if (cafeOfTheDay == null) {
            return;
        }
        cafeOfTheWeekNominations.add(cafeOfTheDay);
    }

    @Scheduled(cron = "0 0 8 ? * THU", zone = "GMT+7")
    public void postCafeOfTheWeekNominations() {
        String cafeOfTheWeekNominationsTweetContent = generateCafeOfTheWeekNominationsTweetContent();
        moveNominationsToPollDictionary();
        cafeOfTheWeekNominationsTweet = twitter.updateStatus(cafeOfTheWeekNominationsTweetContent);
    }

    @Scheduled(cron = "0 0 8 ? * SAT", zone = "GMT+7")
    public void setCafeOfTheWeekWinner() {
        processCafeOfTheWeekPolling();
        deleteCafeOfTheWeekNominationsTweet();
        String cafeOfTheWeek = decideCafeWithHighestPollCount();
        postCafeOfTheWeekWinner(cafeOfTheWeek);
    }

    private List<DailyTweet> getLatestCafeRecommendationTweets() {
        LocalDate yesterday = LocalDate.now(ZoneId.of("GMT+7")).minusDays(1);
        return dailyTweetRepo.findAllByTweetDate(yesterday);
    }

    private String getCafeOfTheDay(List<DailyTweet> cafeRecommendationTweets) {
        int highestCount = 0;
        String cafeOfTheDay = null;
        for (DailyTweet tweet : cafeRecommendationTweets) {
            long tweetId = tweet.getTweetId();
            Status status = twitter.showStatus(tweetId);
            int thisTweetCount = status.getFavoriteCount() + status.getRetweetCount();
            if (thisTweetCount >= highestCount) {
                highestCount = thisTweetCount;
                cafeOfTheDay = tweet.getCafeName() + " - " + tweet.getCafeOwnerTwitterId();
            }
        }
        return cafeOfTheDay;
    }

    private String generateCafeOfTheWeekNominationsTweetContent() {
        StringBuffer buffer = new StringBuffer("[NOMINASI CAFE OF THE WEEK]\nDukung cafe favoritmu"
                + " dengan cara me-reply tweet ini dengan nomor urut cafe pilihanmu!\n\n");
        for (int i = 0; i < cafeOfTheWeekNominations.size(); i++) {
            buffer.append((i + 1) + ". " + cafeOfTheWeekNominations.get(i).split(" - ")[0] + "\n");
        }

        return buffer.toString();
    }

    private void moveNominationsToPollDictionary() {
        cafeOfTheWeekPollCount = new HashMap<>();
        cafeOfTheWeekNominationsData = new HashMap<>();
        int i = 1;
        while (!cafeOfTheWeekNominations.isEmpty()) {
            String cafeData = cafeOfTheWeekNominations.get(0);
            cafeOfTheWeekPollCount.put(i, 0);
            cafeOfTheWeekNominationsData.put(i, cafeData);
            cafeOfTheWeekNominations.remove(cafeData);
            i++;
        }
    }

    private void processCafeOfTheWeekPolling() {
        Query query = new Query("to:hellocupbot since_id:" +
                cafeOfTheWeekNominationsTweet.getId());
        QueryResult results;

        while (query != null) {
            results = twitter.search(query);
            List<Status> tweetReplies = results.getTweets();
            doCountingPoll(tweetReplies);
            query = results.nextQuery();
        }
    }

    private void deleteCafeOfTheWeekNominationsTweet() {
        twitter.destroyStatus(cafeOfTheWeekNominationsTweet.getId());
    }

    private String decideCafeWithHighestPollCount() {
        int maxPollCount = -1;
        Integer cafeOfTheWeekWinnerOrder = null;
        for (Integer cafeOrder : cafeOfTheWeekPollCount.keySet()) {
            if (cafeOfTheWeekPollCount.get(cafeOrder) > maxPollCount) {
                cafeOfTheWeekWinnerOrder = cafeOrder;
                maxPollCount = cafeOfTheWeekPollCount.get(cafeOrder);
            }
        }
        return cafeOfTheWeekNominationsData.get(cafeOfTheWeekWinnerOrder);
    }

    private void postCafeOfTheWeekWinner(String cafeOfTheWeek) {
        String cafeName = cafeOfTheWeek.split(" - ")[0];
        long ownerId = Long.parseLong(cafeOfTheWeek.split(" - ")[1]);
        CafeId cafeId = new CafeId(ownerId, cafeName);
        try {
            Cafe cafe = cafeDatabaseAdapter.getCafe(cafeId)[0];
            cafeOfTheWeekTweet = twitter.updateStatus("[PEMENANG CAFE OF THE WEEK]\n\n"
                    + "Selamat kepada pemenang minggu ini!\n"
                    + "Nama Cafe : " + cafeName
                    + "\nAlamat : " + cafe.getCafeAddress().toString());
            twitter.sendDirectMessage(ownerId, "Selamat, cafe Anda menjadi pemenang Cafe Of The Week!");
            cafeDatabaseAdapter.incrementCotwWins(cafeId);
        } catch (Exception e) {
            return;
        }
    }

    private void doCountingPoll(List<Status> tweetReplies) {
        for (Status poll : tweetReplies) {
            if (isValidPoll(poll)) {
                String pollText = poll.getText();
                Integer cafeOrder = Integer.parseInt(pollText.substring(pollText.length() - 1));
                cafeOfTheWeekPollCount.put(cafeOrder, cafeOfTheWeekPollCount.get(cafeOrder) + 1);
            }
        }
    }

    private boolean isValidPoll(Status poll) {
        return isReplyToNominationsTweet(poll) && isCafeOfTheWeekNominations(poll);
    }

    private boolean isReplyToNominationsTweet(Status poll) {
        return poll.getInReplyToStatusId() == cafeOfTheWeekNominationsTweet.getId();
    }

    private boolean isCafeOfTheWeekNominations(Status poll) {
        String pollText = poll.getText();
        try {
            int converted = Integer.parseInt(pollText.substring(pollText.length() - 1));
            return cafeOfTheWeekPollCount.containsKey(converted);
        } catch (Exception e) {
            return false;
        }
        
    }

    public List<String> getNominationsList() {
        return cafeOfTheWeekNominations;
    }

    public Status getNominationsTweet() {
        return cafeOfTheWeekNominationsTweet;
    }

    public Status getCafeOfTheWeekTweet() {
        return cafeOfTheWeekTweet;
    }
}
