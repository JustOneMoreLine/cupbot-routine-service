package com.cupbotteam.routineservice.service;

import com.cupbotteam.routineservice.core.CafeId;
import com.cupbotteam.routineservice.core.CafePromo;
import java.util.List;

/**
 * An interface use to interact with database service,
 * particularly CafePromo table.
 */
public interface PromoDatabaseAdapter {

    /**
     * Gets a promo by it's id.
     *
     * @param id , promo's id.
     * @return a list that contains one promo that's requested,
     * if no known promo with that id is found, return an empty
     * list.
     */
    public CafePromo[] getPromoById(long id);

    /**
     * Gets a list of promo from that particular Cafe by
     * the cafe's id.
     *
     * @param cafeId , the cafe's id which the promo belongs to.
     * @return A list of CafePromo of requested cafe.
     */
    public CafePromo[] getCafePromo(CafeId cafeId);

    /**
     * Gets all registered promos.
     *
     * @return A list of CafePromo.
     */
    public CafePromo[] getAllPromo();

    /**
     * Adds a new promo to database.
     *
     * @param cafePromo , the cafe promo to be registerd.
     * @return a cafe promo that have just saved to database.
     */
    public CafePromo addPromo(CafePromo cafePromo);

    /**
     * Adds a list of promo.
     *
     * @param cafePromoList , a list of cafe promos.
     * @return a list of cafe promo that have just saved to database.
     */
    public CafePromo[] addMultiplePromo(List<CafePromo> cafePromoList);

    /**
     * Deletes a promo by it's id.
     *
     * @param id , id of to be deleted promo.
     * @return a promo that have just been deleted from database.
     */
    public CafePromo[] deletePromoById(long id);

    /**
     * Deletes all promo held by that cafe.
     *
     * @param cafeId , cafe's id of to be deleted promos belong to.
     * @return a list of promo that have just been deleted.
     */
    public CafePromo[] deleteCafePromo(CafeId cafeId);

    /**
     * Deletes a list of promo by ids.
     *
     * @param ids , list of promo's id to be deleted.
     * @return a list of promo that have just been deleted.
     */
    public CafePromo[] deleteMultiplePromoById(List<Long> ids);

    /**
     * Deletes all promo that belongs to a list of cafe by it's id.
     *
     * @param idList , a list of cafe's id which to be deleted promos belong to.
     * @return a list of promo that have just been deleted.
     */
    public CafePromo[] deleteMultipleCafePromo(List<CafeId> idList);

    /**
     * Deletes all registered promo.
     *
     * @return a list of promo that have just been deleted.
     */
    public CafePromo[] deleteAllPromo();
}
