package com.cupbotteam.routineservice.service;

import com.cupbotteam.routineservice.core.Cafe;
import com.cupbotteam.routineservice.core.CafeId;
import java.util.List;

/**
 * An interface use to interact with database service,
 * particularly the Cafe table.
 */
public interface CafeDatabaseAdapter {

    /**
     * Use to get a registered cafe based on the requested cafeId.
     *
     * @param cafeId , the requested Cafe's id.
     * @return A list that contains one cafe, if the id doesn't exist
     * returns an empty list.
     */
    public Cafe[] getCafe(CafeId cafeId);

    /**
     * Use to get a registered cafe based on the requested cafe owner's Twitter id.
     *
     * @param cafeOwnerTwitterId , the requested Cafe owner's id.
     * @return A list that contains one or more cafe, if the id doesn't exist
     * returns an empty list.
     */
    public Cafe[] getCafeByOwnerTwitterId(long cafeOwnerTwitterId);

    /**
     * Use to get all registered cafe.
     *
     * @return A list of cafe.
     */
    public Cafe[] getAllCafe();

    /**
     * Add a new cafe to the database.
     *
     * @param cafe , the new registered cafe.
     * @return the cafe saved by database.
     */
    public Cafe addCafe(Cafe cafe);

    /**
     * Adds a list of new cafes.
     *
     * @param listOfCafe , list of cafes.
     * @return list of cafes saved by database.
     */
    public Cafe[] addMultipleCafe(List<Cafe> listOfCafe);

    /**
     * Deletes a cafe.
     *
     * @param cafeId , cafe's id to be deleted.
     * @return a list that contains one cafe that have just been deleted,
     * if there are no known cafe with that cafe id, returns an empty list.
     */
    public Cafe[] deleteCafe(CafeId cafeId);

    /**
     * Delete a list of cafes.
     *
     * @param idList , list of cafes to be deleted.
     * @return a list deleted cafes.
     */
    public Cafe[] deleteMultipleCafe(List<CafeId> idList);

    /**
     * Delete all registered cafes.
     *
     * @return a list of deleted cafes.
     */
    public Cafe[] deleteAllCafe();

    /**
     * Use to increment cafeOfTheWeekWins based on the given cafeId.
     *
     * @param cafeId , the requested Cafe's id.
     * @return A list that contains one cafe, if the id doesn't exist
     * returns an empty list.
     */
    public Cafe[] incrementCotwWins(CafeId cafeId);
}
