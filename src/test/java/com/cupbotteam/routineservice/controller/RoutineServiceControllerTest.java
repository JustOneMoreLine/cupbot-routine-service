package com.cupbotteam.routineservice.controller;

import com.cupbotteam.routineservice.repository.DailyTweetRepo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(RoutineServiceController.class)
public class RoutineServiceControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private DailyTweetRepo dailyTweetRepo;

    @Test
    public void testPing() throws Exception {
        mockMvc.perform(get("/wakeMe")).andExpect(status().isOk());
    }

    @Test
    public void testHome() throws Exception {
        mockMvc.perform(get("/")).andExpect(status().isOk());
    }
}
