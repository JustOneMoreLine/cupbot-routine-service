package com.cupbotteam.routineservice.core;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CafeAddressTest {

    @Test
    public void cafeAddressNoArg() {
        CafeAddress cafeAddress = new CafeAddress();
        assertNull(cafeAddress.getStreet());
        assertNull(cafeAddress.getSubDistrict());
        assertNull(cafeAddress.getDistrict());
        assertNull(cafeAddress.getCity());
        assertNull(cafeAddress.getProvince());
        cafeAddress.setStreet("A");
        assertEquals(cafeAddress.getStreet(), "A");
        cafeAddress.setSubDistrict("B");
        assertEquals(cafeAddress.getSubDistrict(), "B");
        cafeAddress.setDistrict("C");
        assertEquals(cafeAddress.getDistrict(), "C");
        cafeAddress.setCity("D");
        assertEquals(cafeAddress.getCity(), "D");
        cafeAddress.setProvince("E");
        assertEquals(cafeAddress.getProvince(), "E");
    }

    @Test
    public void cafeAddressArg() {
        CafeAddress cafeAddress = new CafeAddress("A", "B", "C", "D", "E");
        assertEquals(cafeAddress.getStreet(), "A");
        assertEquals(cafeAddress.getSubDistrict(), "B");
        assertEquals(cafeAddress.getDistrict(), "C");
        assertEquals(cafeAddress.getCity(), "D");
        assertEquals(cafeAddress.getProvince(), "E");
    }


    @Test
    public void testToString() {
        CafeAddress cafeAddress = new CafeAddress("A", "B", "C", "D", "E");
        assertEquals(cafeAddress.toString(), "A, B, C, D, E");
    }
    @Test
    public void testEquals() {
        CafeAddress a = new CafeAddress("A", "B", "C", "D", "E");
        CafeAddress b = new CafeAddress("A", "B", "C", "D", "E");
        CafeAddress c = new CafeAddress("1", "B", "C", "D", "E");
        CafeAddress d = new CafeAddress("A", "1", "C", "D", "E");
        CafeAddress e = new CafeAddress("A", "B", "1", "D", "E");
        CafeAddress f = new CafeAddress("A", "B", "C", "1", "E");
        CafeAddress g = new CafeAddress("A", "B", "C", "D", "1");
        Object h = new Object();
        assertTrue(a.equals(a));
        assertTrue(a.equals(b));
        assertFalse(a.equals(c));
        assertFalse(a.equals(d));
        assertFalse(a.equals(e));
        assertFalse(a.equals(f));
        assertFalse(a.equals(g));
        assertFalse(a.equals(h));
    }
}
