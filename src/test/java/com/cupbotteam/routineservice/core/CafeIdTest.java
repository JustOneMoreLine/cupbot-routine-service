package com.cupbotteam.routineservice.core;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CafeIdTest {

    @Test
    public void cafeIdNoArg() {
        CafeId cafeId = new CafeId();
        assertEquals(cafeId.getCafeOwnerTwitterId(), 0);
        assertNull(cafeId.getCafeName());
        cafeId.setCafeName("Ola");
        assertEquals(cafeId.getCafeName(), "Ola");
        cafeId.setCafeOwnerTwitterId(190);
        assertEquals(cafeId.getCafeOwnerTwitterId(), 190);
    }

    @Test
    public void cafeIdArg() {
        CafeId cafeId = new CafeId(123, "Ola");
        assertEquals(cafeId.getCafeOwnerTwitterId(), 123);
        assertEquals(cafeId.getCafeName(), "Ola");
    }

    @Test
    public void testEquals() {
        CafeId a = new CafeId(123, "Ola");
        CafeId b = new CafeId(123, "Ola");
        CafeId c = new CafeId(412, "Ola");
        CafeId d = new CafeId(123, "Ana");
        CafeId e = new CafeId(678, "Mana");
        Object f = new Object();
        assertTrue(a.equals(a));
        assertTrue(a.equals(b));
        assertFalse(a.equals(c));
        assertFalse(a.equals(d));
        assertFalse(a.equals(e));
        assertFalse(a.equals(f));
    }
}
