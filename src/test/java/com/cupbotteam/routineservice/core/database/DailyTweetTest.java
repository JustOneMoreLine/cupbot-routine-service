package com.cupbotteam.routineservice.core.database;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.time.LocalDate;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class DailyTweetTest {
    DailyTweet dailyTweet;

    @BeforeEach
    public void setUp() {
        dailyTweet = new DailyTweet(123, "Starback", 456);
    }

    @Test
    public void test() {
        assertEquals(dailyTweet.getCafeName(), "Starback");
        dailyTweet.setCafeName("LOL");
        assertEquals(dailyTweet.getCafeName(), "LOL");

        assertEquals(dailyTweet.getCafeOwnerTwitterId(), 456);
        dailyTweet.setCafeOwnerTwitterId(555);
        assertEquals(dailyTweet.getCafeOwnerTwitterId(), 555);

        assertEquals(dailyTweet.getTweetId(), 123);
        dailyTweet.setTweetId(444);
        assertEquals(dailyTweet.getTweetId(), 444);

        LocalDate time = LocalDate.now();
        dailyTweet.setTweetDate(time);
        assertEquals(dailyTweet.getTweetDate(), time);

        assertEquals(dailyTweet.getZoneId(), "GMT+7");
    }
}
