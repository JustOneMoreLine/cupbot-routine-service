package com.cupbotteam.routineservice.core;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class CrcTokenTest {

    @Test
    public void crcTokenTest() {
        CrcToken token = new CrcToken();
        assertNull(token.getToken());
        token.setToken("ab123");
        assertEquals(token.getToken(), "ab123");
        assertEquals(token.toString(), "Token: ab123");
    }
}
