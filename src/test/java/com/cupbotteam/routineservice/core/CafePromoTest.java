package com.cupbotteam.routineservice.core;

import org.junit.jupiter.api.Test;
import java.time.LocalDateTime;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CafePromoTest {

    public CafeId newCafeId() {
        CafeId result = new CafeId();
        result.setCafeOwnerTwitterId(123);
        result.setCafeName("cetarBak");
        return result;
    }

    @Test
    public void cafePromoNoArg() {
        CafePromo cafePromo = new CafePromo();

        assertEquals(cafePromo.getId(), 0);
        assertNull(cafePromo.getCafeId());
        assertNull(cafePromo.getStartOfPromo());
        assertNull(cafePromo.getEndOfPromo());
        assertNull(cafePromo.getPromoCode());
        assertNull(cafePromo.getPromoDescription());

        cafePromo.setId(1);
        assertEquals(cafePromo.getId(), 1);

        CafeId cafeId = newCafeId();
        cafePromo.setCafeId(cafeId);
        assertEquals(cafePromo.getCafeId(), cafeId);

        LocalDateTime time = LocalDateTime.now();
        cafePromo.setStartOfPromo(time);
        assertEquals(cafePromo.getStartOfPromo(), time);

        cafePromo.setEndOfPromo(time);
        assertEquals(cafePromo.getEndOfPromo(), time);

        cafePromo.setPromoCode("AAAA");
        assertEquals(cafePromo.getPromoCode(), "AAAA");

        cafePromo.setPromoDescription("BBB");
        assertEquals(cafePromo.getPromoDescription(), "BBB");

    }

    @Test
    public void cafePromoArg() {
        CafeId cafeId = newCafeId();
        LocalDateTime time = LocalDateTime.now();
        CafePromo cafePromo = new CafePromo(15, cafeId, "AAA", time, time, "BBB");
        assertEquals(cafePromo.getId(), 15);
        assertEquals(cafePromo.getCafeId(), cafeId);
        assertEquals(cafePromo.getPromoCode(), "AAA");
        assertEquals(cafePromo.getStartOfPromo(), time);
        assertEquals(cafePromo.getEndOfPromo(), time);
        assertEquals(cafePromo.getPromoDescription(), "BBB");
    }

    @Test
    public void testEquals() {
        CafeId id1 = new CafeId(123, "Ola");
        CafeId id2 = new CafeId(567, "Alo");
        LocalDateTime date = LocalDateTime.now();
        CafePromo a = new CafePromo(123, id1, "A", date, date, "B");
        CafePromo b = new CafePromo(123, id1, "A", date, date, "B");
        CafePromo c = new CafePromo(456, id1, "A", date, date, "B");
        CafePromo d = new CafePromo(123, id2, "A", date, date, "B");
        CafePromo e = new CafePromo(123, id1, "C", date, date, "B");
        CafePromo f = new CafePromo(123, id1, "A", date.minusDays(1), date, "B");
        CafePromo g = new CafePromo(123, id1, "A", date, date.minusDays(1), "B");
        CafePromo h = new CafePromo(123, id1, "A", date, date, "C");
        Object i = new Object();
        assertTrue(a.equals(a));
        assertTrue(a.equals(b));
        assertFalse(a.equals(c));
        assertFalse(a.equals(d));
        assertFalse(a.equals(e));
        assertFalse(a.equals(f));
        assertFalse(a.equals(g));
        assertFalse(a.equals(h));
        assertFalse(a.equals(i));
    }
}
