package com.cupbotteam.routineservice.core;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CafeTest {

    public CafeId newCafeId() {
        CafeId result = new CafeId();
        result.setCafeOwnerTwitterId(123);
        result.setCafeName("cetarBak");
        return result;
    }

    public CafeAddress newAddress() {
        CafeAddress result = new CafeAddress();
        result.setStreet("A");
        result.setSubDistrict("B");
        result.setDistrict("C");
        result.setCity("D");
        result.setProvince("E");
        return result;
    }

    @Test
    public void testCafeNoArg() {
        CafeId cafeId = newCafeId();
        CafeAddress cafeAddress = newAddress();
        Cafe cafeNoArg = new Cafe();
        cafeNoArg.setId(cafeId);
        assertEquals(cafeNoArg.getId(), cafeId);
        cafeNoArg.setCafeAddress(cafeAddress);
        assertEquals(cafeNoArg.getCafeAddress(), cafeAddress);
        assertEquals(cafeNoArg.getCafeOfTheWeekWins(), 0);
        cafeNoArg.setCafeOfTheWeekWins(10);
        assertEquals(cafeNoArg.getCafeOfTheWeekWins(), 10);
        assertEquals(cafeNoArg.getCafeOfAllTimeRank(), Long.MAX_VALUE);
        cafeNoArg.setCafeOfAllTimeRank(4);
        assertEquals(cafeNoArg.getCafeOfAllTimeRank(), 4);
    }

    @Test
    public void testCafeArg() {
        CafeId cafeId = newCafeId();
        CafeAddress cafeAddress = newAddress();
        Cafe cafeArg = new Cafe(cafeId, cafeAddress);
        assertEquals(cafeArg.getId(), cafeId);
        assertEquals(cafeArg.getCafeAddress(), cafeAddress);
        assertEquals(cafeArg.getCafeOfTheWeekWins(), 0);
        assertEquals(cafeArg.getCafeOfAllTimeRank(), Long.MAX_VALUE);
    }

    @Test
    public void testEquals() {
        CafeId id1a = new CafeId(123, "Ola");
        CafeId id1b = new CafeId(123, "Ola");
        CafeId id2 = new CafeId(678, "Mana");
        CafeAddress add1a = new CafeAddress("A", "B", "C", "D", "E");
        CafeAddress add1b = new CafeAddress("A", "B", "C", "D", "E");
        CafeAddress add2 = new CafeAddress("1", "B", "C", "D", "E");
        Cafe a = new Cafe(id1a, add1a);
        Cafe b = new Cafe(id1b, add1b);
        Cafe c = new Cafe(id2, add1a);
        Cafe d = new Cafe(id1a, add2);
        Object e = new Object();
        assertTrue(a.equals(a));
        assertTrue(a.equals(b));
        assertFalse(a.equals(c));
        assertFalse(a.equals(d));
        assertFalse(a.equals(e));
    }
}
