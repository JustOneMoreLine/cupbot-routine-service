package com.cupbotteam.routineservice.service;

import com.cupbotteam.routineservice.core.CafeId;
import com.cupbotteam.routineservice.core.CafePromo;
import com.cupbotteam.routineservice.core.CrcToken;
import com.cupbotteam.routineservice.utility.Sha256;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

public class PromoDatabaseAdapterImplTest {
    PromoDatabaseAdapter promoDatabaseAdapter;
    @Mock
    DiscoveryClient discoveryClient;
    @Mock
    WebClient webClient;
    @Mock
    RestTemplate restTemplate;
    @Spy
    WebClient.RequestBodySpec requestBodySpec;
    @Spy
    WebClient.RequestBodyUriSpec requestBodyUriSpec;
    @Spy
    WebClient.RequestHeadersSpec requestHeadersSpec;
    @Spy
    WebClient.ResponseSpec responseSpec;

    public CafeId newCafeId() {
        CafeId cafeId = new CafeId();
        cafeId.setCafeOwnerTwitterId(123);
        cafeId.setCafeName("ABCD");
        return cafeId;
    }

    public CrcToken newDummyToken() {
        CrcToken crcToken = new CrcToken();
        crcToken.setToken("ABCDE");
        return crcToken;
    }

    public CafePromo newPromo() {
        CafePromo cafePromo = new CafePromo();
        cafePromo.setId(12345);
        cafePromo.setCafeId(newCafeId());
        cafePromo.setPromoCode("ABCDE");
        cafePromo.setPromoDescription("EFGHI");
        cafePromo.setStartOfPromo(LocalDateTime.now());
        cafePromo.setEndOfPromo(LocalDateTime.now().plusHours(1));
        return cafePromo;
    }
    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        try {
            URI uri = new URI("https://dummyURI.com");
            List<String> serviceList = new ArrayList<>();
            serviceList.add("database");
            List<ServiceInstance> serviceInstances = new ArrayList<>();
            ServiceInstance serviceInstance = new ServiceInstance() {
                @Override
                public String getServiceId() {
                    return "database";
                }

                @Override
                public String getHost() {
                    return "dummyURI";
                }

                @Override
                public int getPort() {
                    return 0;
                }

                @Override
                public boolean isSecure() {
                    return false;
                }

                @Override
                public java.net.URI getUri() {
                    return uri;
                }

                @Override
                public Map<String, String> getMetadata() {
                    return null;
                }
            };
            serviceInstances.add(serviceInstance);
            doReturn(serviceList).when(discoveryClient).getServices();
            doReturn(serviceInstances).when(discoveryClient).getInstances("database");
            promoDatabaseAdapter = new PromoDatabaseAdapterImpl(discoveryClient, webClient, restTemplate);
        } catch (URISyntaxException e) {

        }
    }

    @Test
    public void testGetPromoById() {
        CafePromo[] promos = new CafePromo[1];
        promos[0] = newPromo();
        Mono<CafePromo[]> mono = Mono.just(promos);
        doReturn(newDummyToken()).when(restTemplate).getForObject("https://dummyURI.com/challange", CrcToken.class);
        doReturn(requestBodyUriSpec).when(webClient).post();
        doReturn(requestBodySpec).when(requestBodyUriSpec).uri("/getPromoById?response=" + Sha256.hash("ABCDE"));
        doReturn(requestBodySpec).when(requestBodySpec).accept(MediaType.APPLICATION_JSON);
        doReturn(requestHeadersSpec).when(requestBodySpec).bodyValue(Long.parseLong("12345"));
        doReturn(responseSpec).when(requestHeadersSpec).retrieve();
        doReturn(mono).when(responseSpec).bodyToMono(CafePromo[].class);

        CafePromo[] retrievedData = promoDatabaseAdapter.getPromoById(12345);
        verify(webClient).post();
        assertEquals(retrievedData.length, 1);
        assertEquals(retrievedData[0], promos[0]);
    }

    @Test
    public void testGetCafePromo() {
        CafePromo[] promos = new CafePromo[1];
        promos[0] = newPromo();
        Mono<CafePromo[]> mono = Mono.just(promos);
        doReturn(newDummyToken()).when(restTemplate).getForObject("https://dummyURI.com/challange", CrcToken.class);
        doReturn(requestBodyUriSpec).when(webClient).post();
        doReturn(requestBodySpec).when(requestBodyUriSpec).uri("/getCafePromo?response=" + Sha256.hash("ABCDE"));
        doReturn(requestBodySpec).when(requestBodySpec).accept(MediaType.APPLICATION_JSON);
        doReturn(requestHeadersSpec).when(requestBodySpec).bodyValue(newCafeId());
        doReturn(responseSpec).when(requestHeadersSpec).retrieve();
        doReturn(mono).when(responseSpec).bodyToMono(CafePromo[].class);

        CafePromo[] retrievedData = promoDatabaseAdapter.getCafePromo(newCafeId());
        verify(webClient).post();
        assertEquals(retrievedData.length, 1);
        assertEquals(retrievedData[0], promos[0]);
    }

    @Test
    public void testGetAllPromo() {
        CafePromo[] promos = new CafePromo[1];
        promos[0] = newPromo();
        Mono<CafePromo[]> mono = Mono.just(promos);
        doReturn(newDummyToken()).when(restTemplate).getForObject("https://dummyURI.com/challange", CrcToken.class);
        doReturn(requestBodyUriSpec).when(webClient).get();
        doReturn(requestBodySpec).when(requestBodyUriSpec).uri("/getAllPromo?response=" + Sha256.hash("ABCDE"));
        doReturn(requestBodySpec).when(requestBodySpec).accept(MediaType.APPLICATION_JSON);
        doReturn(responseSpec).when(requestBodySpec).retrieve();
        doReturn(mono).when(responseSpec).bodyToMono(CafePromo[].class);

        CafePromo[] retrievedData = promoDatabaseAdapter.getAllPromo();
        verify(webClient).get();
        assertEquals(retrievedData.length, promos.length);
        for (int i = 0; i < retrievedData.length; i++) {
            assertEquals(retrievedData[i], promos[i]);
        }
    }

    @Test
    public void testAddPromo() {
        CafePromo promo = newPromo();
        Mono<CafePromo> mono = Mono.just(promo);
        doReturn(newDummyToken()).when(restTemplate).getForObject("https://dummyURI.com/challange", CrcToken.class);
        doReturn(requestBodyUriSpec).when(webClient).post();
        doReturn(requestBodySpec).when(requestBodyUriSpec).uri("/addPromo?response=" + Sha256.hash("ABCDE"));
        doReturn(requestBodySpec).when(requestBodySpec).accept(MediaType.APPLICATION_JSON);
        doReturn(requestHeadersSpec).when(requestBodySpec).bodyValue(promo);
        doReturn(responseSpec).when(requestHeadersSpec).retrieve();
        doReturn(mono).when(responseSpec).bodyToMono(CafePromo.class);

        CafePromo retrievedData = promoDatabaseAdapter.addPromo(promo);
        verify(webClient).post();
        assertEquals(retrievedData, promo);
    }

    @Test
    public void testAddMultiplePromo() {
        CafePromo[] promos = new CafePromo[2];
        promos[0] = newPromo();
        promos[1] = newPromo();
        List<CafePromo> promosInput = new ArrayList<>();
        promosInput.add(promos[0]);
        promosInput.add(promos[1]);
        Mono<CafePromo[]> mono = Mono.just(promos);
        // mock return for Response Generator
        doReturn(newDummyToken()).when(restTemplate).getForObject("https://dummyURI.com/challange", CrcToken.class);

        // mock return for WebClient
        doReturn(requestBodyUriSpec).when(webClient).post();
        doReturn(requestBodySpec).when(requestBodyUriSpec).uri("/addMultiplePromo?response=" + Sha256.hash("ABCDE"));
        doReturn(requestBodySpec).when(requestBodySpec).accept(MediaType.APPLICATION_JSON);
        doReturn(requestHeadersSpec).when(requestBodySpec).bodyValue(promosInput);
        doReturn(responseSpec).when(requestHeadersSpec).retrieve();
        doReturn(mono).when(responseSpec).bodyToMono(CafePromo[].class);

        CafePromo[] retrievedData = promoDatabaseAdapter.addMultiplePromo(promosInput);
        verify(webClient).post();
        assertEquals(retrievedData.length, promos.length);
        for (int i = 0; i < retrievedData.length; i++) {
            assertEquals(retrievedData[i], promos[i]);
        }
    }

    @Test
    public void testDeletePromoById() {
        CafePromo[] promos = new CafePromo[1];
        promos[0] = newPromo();
        Mono<CafePromo[]> mono = Mono.just(promos);
        doReturn(newDummyToken()).when(restTemplate).getForObject("https://dummyURI.com/challange", CrcToken.class);
        doReturn(requestBodyUriSpec).when(webClient).post();
        doReturn(requestBodySpec).when(requestBodyUriSpec).uri("/deletePromoById?response=" + Sha256.hash("ABCDE"));
        doReturn(requestBodySpec).when(requestBodySpec).accept(MediaType.APPLICATION_JSON);
        doReturn(requestHeadersSpec).when(requestBodySpec).bodyValue(Long.parseLong("12345"));
        doReturn(responseSpec).when(requestHeadersSpec).retrieve();
        doReturn(mono).when(responseSpec).bodyToMono(CafePromo[].class);

        CafePromo[] retrievedData = promoDatabaseAdapter.deletePromoById(12345);
        verify(webClient).post();
        assertEquals(retrievedData.length, 1);
        assertEquals(retrievedData[0], promos[0]);
    }

    @Test
    public void testDeleteCafePromo() {
        CafePromo[] promos = new CafePromo[1];
        promos[0] = newPromo();
        Mono<CafePromo[]> mono = Mono.just(promos);
        doReturn(newDummyToken()).when(restTemplate).getForObject("https://dummyURI.com/challange", CrcToken.class);
        doReturn(requestBodyUriSpec).when(webClient).post();
        doReturn(requestBodySpec).when(requestBodyUriSpec).uri("/deleteCafePromo?response=" + Sha256.hash("ABCDE"));
        doReturn(requestBodySpec).when(requestBodySpec).accept(MediaType.APPLICATION_JSON);
        doReturn(requestHeadersSpec).when(requestBodySpec).bodyValue(newCafeId());
        doReturn(responseSpec).when(requestHeadersSpec).retrieve();
        doReturn(mono).when(responseSpec).bodyToMono(CafePromo[].class);

        CafePromo[] retrievedData = promoDatabaseAdapter.deleteCafePromo(newCafeId());
        verify(webClient).post();
        assertEquals(retrievedData.length, 1);
        assertEquals(retrievedData[0], promos[0]);
    }

    @Test
    public void testDeleteMultiplePromoById() {
        CafePromo[] promos = new CafePromo[2];
        promos[0] = newPromo();
        promos[1] = newPromo();
        List<Long> promosInput = new ArrayList<>();
        promosInput.add(Long.parseLong("12345"));
        promosInput.add(Long.parseLong("12345"));
        Mono<CafePromo[]> mono = Mono.just(promos);
        // mock return for Response Generator
        doReturn(newDummyToken()).when(restTemplate).getForObject("https://dummyURI.com/challange", CrcToken.class);

        // mock return for WebClient
        doReturn(requestBodyUriSpec).when(webClient).post();
        doReturn(requestBodySpec)
                .when(requestBodyUriSpec)
                .uri("/deleteMultiplePromoById?response=" + Sha256.hash("ABCDE"));
        doReturn(requestBodySpec).when(requestBodySpec).accept(MediaType.APPLICATION_JSON);
        doReturn(requestHeadersSpec).when(requestBodySpec).bodyValue(promosInput);
        doReturn(responseSpec).when(requestHeadersSpec).retrieve();
        doReturn(mono).when(responseSpec).bodyToMono(CafePromo[].class);

        CafePromo[] retrievedData = promoDatabaseAdapter.deleteMultiplePromoById(promosInput);
        verify(webClient).post();
        assertEquals(retrievedData.length, promosInput.size());
        for (int i = 0; i < retrievedData.length; i++) {
            assertEquals(retrievedData[i], promos[i]);
        }
    }

    @Test
    public void testDeleteMultipleCafePromo() {
        CafePromo[] promos = new CafePromo[2];
        promos[0] = newPromo();
        promos[1] = newPromo();
        List<CafeId> promosInput = new ArrayList<>();
        promosInput.add(newCafeId());
        promosInput.add(newCafeId());
        Mono<CafePromo[]> mono = Mono.just(promos);
        // mock return for Response Generator
        doReturn(newDummyToken()).when(restTemplate).getForObject("https://dummyURI.com/challange", CrcToken.class);

        // mock return for WebClient
        doReturn(requestBodyUriSpec).when(webClient).post();
        doReturn(requestBodySpec)
                .when(requestBodyUriSpec)
                .uri("/deleteMultipleCafePromo?response=" + Sha256.hash("ABCDE"));
        doReturn(requestBodySpec).when(requestBodySpec).accept(MediaType.APPLICATION_JSON);
        doReturn(requestHeadersSpec).when(requestBodySpec).bodyValue(promosInput);
        doReturn(responseSpec).when(requestHeadersSpec).retrieve();
        doReturn(mono).when(responseSpec).bodyToMono(CafePromo[].class);

        CafePromo[] retrievedData = promoDatabaseAdapter.deleteMultipleCafePromo(promosInput);
        verify(webClient).post();
        assertEquals(retrievedData.length, promosInput.size());
        for (int i = 0; i < retrievedData.length; i++) {
            assertEquals(retrievedData[i], promos[i]);
        }
    }

    @Test
    public void testDeleteAllPromo() {
        CafePromo[] promos = new CafePromo[1];
        promos[0] = newPromo();
        Mono<CafePromo[]> mono = Mono.just(promos);
        doReturn(newDummyToken()).when(restTemplate).getForObject("https://dummyURI.com/challange", CrcToken.class);
        doReturn(requestBodyUriSpec).when(webClient).get();
        doReturn(requestBodySpec).when(requestBodyUriSpec).uri("/deleteAllPromo?response=" + Sha256.hash("ABCDE"));
        doReturn(requestBodySpec).when(requestBodySpec).accept(MediaType.APPLICATION_JSON);
        doReturn(responseSpec).when(requestBodySpec).retrieve();
        doReturn(mono).when(responseSpec).bodyToMono(CafePromo[].class);

        CafePromo[] retrievedData = promoDatabaseAdapter.deleteAllPromo();
        verify(webClient).get();
        assertEquals(retrievedData.length, promos.length);
        for (int i = 0; i < retrievedData.length; i++) {
            assertEquals(retrievedData[i], promos[i]);
        }
    }
}
