package com.cupbotteam.routineservice.service;

import com.cupbotteam.routineservice.core.Cafe;
import com.cupbotteam.routineservice.core.CafeAddress;
import com.cupbotteam.routineservice.core.CafeId;
import com.cupbotteam.routineservice.core.CrcToken;
import com.cupbotteam.routineservice.utility.Sha256;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

public class CafeDatabaseAdapterImplTest {
    CafeDatabaseAdapter cafeDatabaseAdapter;
    @Mock
    DiscoveryClient discoveryClient;
    @Mock
    WebClient webClient;
    @Mock
    RestTemplate restTemplate;
    @Spy
    WebClient.RequestBodySpec requestBodySpec;
    @Spy
    WebClient.RequestBodyUriSpec requestBodyUriSpec;
    @Spy
    WebClient.RequestHeadersSpec requestHeadersSpec;
    @Spy
    WebClient.ResponseSpec responseSpec;

    public CafeId newCafeId() {
        CafeId cafeId = new CafeId();
        cafeId.setCafeOwnerTwitterId(123);
        cafeId.setCafeName("ABCD");
        return cafeId;
    }

    public CafeAddress newCafeAddress() {
        CafeAddress cafeAddress = new CafeAddress();
        cafeAddress.setStreet("A");
        cafeAddress.setSubDistrict("B");
        cafeAddress.setDistrict("C");
        cafeAddress.setCity("D");
        cafeAddress.setProvince("E");
        return cafeAddress;
    }

    public Cafe newCafe() {
        Cafe cafe = new Cafe();
        cafe.setId(newCafeId());
        cafe.setCafeAddress(newCafeAddress());
        cafe.setCafeOfAllTimeRank(1);
        cafe.setCafeOfTheWeekWins(1);
        return cafe;
    }

    public CrcToken newDummyToken() {
        CrcToken crcToken = new CrcToken();
        crcToken.setToken("ABCDE");
        return crcToken;
    }

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        try {
            URI uri = new URI("https://dummyURI.com");
            List<String> serviceList = new ArrayList<>();
            serviceList.add("database");
            List<ServiceInstance> serviceInstances = new ArrayList<>();
            ServiceInstance serviceInstance = new ServiceInstance() {
                @Override
                public String getServiceId() {
                    return "database";
                }

                @Override
                public String getHost() {
                    return "dummyURI";
                }

                @Override
                public int getPort() {
                    return 0;
                }

                @Override
                public boolean isSecure() {
                    return false;
                }

                @Override
                public java.net.URI getUri() {
                    return uri;
                }

                @Override
                public Map<String, String> getMetadata() {
                    return null;
                }
            };
            serviceInstances.add(serviceInstance);
            doReturn(serviceList).when(discoveryClient).getServices();
            doReturn(serviceInstances).when(discoveryClient).getInstances("database");
            cafeDatabaseAdapter = new CafeDatabaseAdapterImpl(discoveryClient, webClient, restTemplate);
        } catch (URISyntaxException e) {

        }
    }

    @Test
    public void testGetCafe() {
        Cafe[] cafes = new Cafe[1];
        cafes[0] = newCafe();
        Mono<Cafe[]> mono = Mono.just(cafes);
        doReturn(newDummyToken()).when(restTemplate).getForObject("https://dummyURI.com/challange", CrcToken.class);
        doReturn(requestBodyUriSpec).when(webClient).post();
        doReturn(requestBodySpec).when(requestBodyUriSpec).uri("/getCafe?response=" + Sha256.hash("ABCDE"));
        doReturn(requestBodySpec).when(requestBodySpec).accept(MediaType.APPLICATION_JSON);
        doReturn(requestHeadersSpec).when(requestBodySpec).bodyValue(newCafeId());
        doReturn(responseSpec).when(requestHeadersSpec).retrieve();
        doReturn(mono).when(responseSpec).bodyToMono(Cafe[].class);

        Cafe[] retrievedData = cafeDatabaseAdapter.getCafe(newCafeId());
        verify(webClient).post();
        assertEquals(retrievedData[0], newCafe());
    }

    @Test
    public void testGetCafeByOwnerTwitterId() {
        Cafe[] cafes = new Cafe[1];
        cafes[0] = newCafe();
        Mono<Cafe[]> mono = Mono.just(cafes);
        // mock return for Response Generator
        doReturn(newDummyToken()).when(restTemplate).getForObject("https://dummyURI.com/challange", CrcToken.class);

        // mock return for WebClient
        doReturn(requestBodyUriSpec).when(webClient).post();
        doReturn(requestBodySpec)
                .when(requestBodyUriSpec)
                .uri("/getCafeByOwnerTwitterId?response=" + Sha256.hash("ABCDE"));
        doReturn(requestBodySpec).when(requestBodySpec).accept(MediaType.APPLICATION_JSON);
        doReturn(requestHeadersSpec).when(requestBodySpec).bodyValue(Long.parseLong("123"));
        doReturn(responseSpec).when(requestHeadersSpec).retrieve();
        doReturn(mono).when(responseSpec).bodyToMono(Cafe[].class);

        Cafe[] retrievedData = cafeDatabaseAdapter.getCafeByOwnerTwitterId(123);
        verify(webClient).post();
        assertEquals(retrievedData[0], newCafe());
    }

    @Test
    public void testGetAllCafe() {
        Cafe[] cafes = new Cafe[2];
        cafes[0] = newCafe();
        cafes[1] = newCafe();
        Mono<Cafe[]> mono = Mono.just(cafes);
        // mock return for Response Generator
        doReturn(newDummyToken()).when(restTemplate).getForObject("https://dummyURI.com/challange", CrcToken.class);

        // mock return for WebClient
        doReturn(requestBodyUriSpec).when(webClient).get();
        doReturn(requestBodySpec).when(requestBodyUriSpec).uri("/getAllCafe?response=" + Sha256.hash("ABCDE"));
        doReturn(requestBodySpec).when(requestBodySpec).accept(MediaType.APPLICATION_JSON);
        doReturn(responseSpec).when(requestBodySpec).retrieve();
        doReturn(mono).when(responseSpec).bodyToMono(Cafe[].class);

        Cafe[] retrievedData = cafeDatabaseAdapter.getAllCafe();
        verify(webClient).get();
        assertEquals(retrievedData.length, 2);
        assertEquals(retrievedData[0], newCafe());
    }

    @Test
    public void testAddCafe() {
        Cafe[] cafes = new Cafe[1];
        Cafe cafe = newCafe();
        cafes[0] = newCafe();
        Mono<Cafe> mono = Mono.just(cafe);
        // mock return for Response Generator
        doReturn(newDummyToken()).when(restTemplate).getForObject("https://dummyURI.com/challange", CrcToken.class);

        // mock return for WebClient
        doReturn(requestBodyUriSpec).when(webClient).post();
        doReturn(requestBodySpec).when(requestBodyUriSpec).uri("/addCafe?response=" + Sha256.hash("ABCDE"));
        doReturn(requestBodySpec).when(requestBodySpec).accept(MediaType.APPLICATION_JSON);
        doReturn(requestHeadersSpec).when(requestBodySpec).bodyValue(cafe);
        doReturn(responseSpec).when(requestHeadersSpec).retrieve();
        doReturn(mono).when(responseSpec).bodyToMono(Cafe.class);

        Cafe retrievedData = cafeDatabaseAdapter.addCafe(cafe);
        verify(webClient).post();
        assertEquals(retrievedData, newCafe());
    }

    @Test
    public void testAddMultipleCafe() {
        Cafe[] cafes = new Cafe[2];
        cafes[0] = newCafe();
        cafes[1] = newCafe();
        List<Cafe> cafesInput = new ArrayList<>();
        cafesInput.add(newCafe());
        cafesInput.add(newCafe());
        Mono<Cafe[]> mono = Mono.just(cafes);
        // mock return for Response Generator
        doReturn(newDummyToken()).when(restTemplate).getForObject("https://dummyURI.com/challange", CrcToken.class);

        // mock return for WebClient
        doReturn(requestBodyUriSpec).when(webClient).post();
        doReturn(requestBodySpec).when(requestBodyUriSpec).uri("/addMultipleCafe?response=" + Sha256.hash("ABCDE"));
        doReturn(requestBodySpec).when(requestBodySpec).accept(MediaType.APPLICATION_JSON);
        doReturn(requestHeadersSpec).when(requestBodySpec).bodyValue(cafesInput);
        doReturn(responseSpec).when(requestHeadersSpec).retrieve();
        doReturn(mono).when(responseSpec).bodyToMono(Cafe[].class);

        Cafe[] retrievedData = cafeDatabaseAdapter.addMultipleCafe(cafesInput);
        verify(webClient).post();
        assertEquals(retrievedData.length, cafesInput.size());
        for (int i = 0; i < retrievedData.length; i++) {
            assertEquals(retrievedData[i], cafesInput.get(i));
        }
    }

    @Test
    public void testDeleteCafe() {
        Cafe[] cafes = new Cafe[1];
        CafeId cafeId = newCafeId();
        cafes[0] = newCafe();
        Mono<Cafe[]> mono = Mono.just(cafes);
        // mock return for Response Generator
        doReturn(newDummyToken()).when(restTemplate).getForObject("https://dummyURI.com/challange", CrcToken.class);

        // mock return for WebClient
        doReturn(requestBodyUriSpec).when(webClient).post();
        doReturn(requestBodySpec).when(requestBodyUriSpec).uri("/deleteCafe?response=" + Sha256.hash("ABCDE"));
        doReturn(requestBodySpec).when(requestBodySpec).accept(MediaType.APPLICATION_JSON);
        doReturn(requestHeadersSpec).when(requestBodySpec).bodyValue(cafeId);
        doReturn(responseSpec).when(requestHeadersSpec).retrieve();
        doReturn(mono).when(responseSpec).bodyToMono(Cafe[].class);

        Cafe[] retrievedData = cafeDatabaseAdapter.deleteCafe(cafeId);
        verify(webClient).post();
        assertEquals(retrievedData.length, 1);
        assertEquals(retrievedData[0], newCafe());
    }

    @Test
    public void testDeleteMultipleCafe() {
        Cafe[] cafes = new Cafe[2];
        cafes[0] = newCafe();
        cafes[1] = newCafe();
        List<CafeId> cafesInput = new ArrayList<>();
        cafesInput.add(newCafeId());
        cafesInput.add(newCafeId());
        Mono<Cafe[]> mono = Mono.just(cafes);
        // mock return for Response Generator
        doReturn(newDummyToken()).when(restTemplate).getForObject("https://dummyURI.com/challange", CrcToken.class);

        // mock return for WebClient
        doReturn(requestBodyUriSpec).when(webClient).post();
        doReturn(requestBodySpec).when(requestBodyUriSpec).uri("/deleteMultipleCafe?response=" + Sha256.hash("ABCDE"));
        doReturn(requestBodySpec).when(requestBodySpec).accept(MediaType.APPLICATION_JSON);
        doReturn(requestHeadersSpec).when(requestBodySpec).bodyValue(cafesInput);
        doReturn(responseSpec).when(requestHeadersSpec).retrieve();
        doReturn(mono).when(responseSpec).bodyToMono(Cafe[].class);

        Cafe[] retrievedData = cafeDatabaseAdapter.deleteMultipleCafe(cafesInput);
        verify(webClient).post();
        assertEquals(retrievedData.length, cafes.length);
        for (int i = 0; i < retrievedData.length; i++) {
            assertEquals(retrievedData[i], cafes[i]);
        }
    }

    @Test
    public void deleteAllCafe() {
        Cafe[] cafes = new Cafe[2];
        cafes[0] = newCafe();
        cafes[1] = newCafe();
        List<Cafe> cafesInput = new ArrayList<>();
        cafesInput.add(newCafe());
        cafesInput.add(newCafe());
        Mono<Cafe[]> mono = Mono.just(cafes);
        // mock return for Response Generator
        doReturn(newDummyToken()).when(restTemplate).getForObject("https://dummyURI.com/challange", CrcToken.class);

        // mock return for WebClient
        doReturn(requestBodyUriSpec).when(webClient).get();
        doReturn(requestBodySpec).when(requestBodyUriSpec).uri("/deleteAllCafe?response=" + Sha256.hash("ABCDE"));
        doReturn(requestBodySpec).when(requestBodySpec).accept(MediaType.APPLICATION_JSON);
        doReturn(responseSpec).when(requestBodySpec).retrieve();
        doReturn(mono).when(responseSpec).bodyToMono(Cafe[].class);

        Cafe[] retrievedData = cafeDatabaseAdapter.deleteAllCafe();
        verify(webClient).get();
        assertEquals(retrievedData.length, cafesInput.size());
        for (int i = 0; i < retrievedData.length; i++) {
            assertEquals(retrievedData[i], cafesInput.get(i));
        }
    }

    @Test
    public void testIncrementCotwWins() {
        Cafe[] cafes = new Cafe[1];
        cafes[0] = newCafe();
        Mono<Cafe[]> mono = Mono.just(cafes);
        doReturn(newDummyToken()).when(restTemplate).getForObject("https://dummyURI.com/challange", CrcToken.class);
        doReturn(requestBodyUriSpec).when(webClient).post();
        doReturn(requestBodySpec).when(requestBodyUriSpec).uri("/incrementCotwWins?response=" + Sha256.hash("ABCDE"));
        doReturn(requestBodySpec).when(requestBodySpec).accept(MediaType.APPLICATION_JSON);
        doReturn(requestHeadersSpec).when(requestBodySpec).bodyValue(newCafeId());
        doReturn(responseSpec).when(requestHeadersSpec).retrieve();
        doReturn(mono).when(responseSpec).bodyToMono(Cafe[].class);

        Cafe[] retrievedData = cafeDatabaseAdapter.incrementCotwWins(newCafeId());
        verify(webClient).post();
        assertEquals(retrievedData[0], newCafe());
    }
}
