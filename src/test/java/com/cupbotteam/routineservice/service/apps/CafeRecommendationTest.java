package com.cupbotteam.routineservice.service.apps;

import com.cupbotteam.routineservice.core.Cafe;
import com.cupbotteam.routineservice.core.CafeAddress;
import com.cupbotteam.routineservice.core.CafeId;
import com.cupbotteam.routineservice.core.database.DailyTweet;
import com.cupbotteam.routineservice.repository.DailyTweetRepo;
import com.cupbotteam.routineservice.service.CafeDatabaseAdapter;
import com.cupbotteam.routineservice.service.Twitter4JAdapter;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import twitter4j.GeoLocation;
import twitter4j.HashtagEntity;
import twitter4j.MediaEntity;
import twitter4j.Place;
import twitter4j.RateLimitStatus;
import twitter4j.Scopes;
import twitter4j.Status;
import twitter4j.SymbolEntity;
import twitter4j.URLEntity;
import twitter4j.User;
import twitter4j.UserMentionEntity;
import java.util.Date;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.atMost;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

public class CafeRecommendationTest {
    @InjectMocks
    CafeRecommendation cafeRecommendation;
    @Spy
    CafeDatabaseAdapter cafeDatabaseAdapter;
    @Spy
    Twitter4JAdapter twitter;
    @Spy
    DailyTweetRepo dailyTweetRepo;

    public CafeId newCafeId() {
        CafeId cafeId = new CafeId();
        cafeId.setCafeOwnerTwitterId(123);
        cafeId.setCafeName("ABCD");
        return cafeId;
    }

    public CafeAddress newCafeAddress() {
        CafeAddress cafeAddress = new CafeAddress();
        cafeAddress.setStreet("A");
        cafeAddress.setSubDistrict("B");
        cafeAddress.setDistrict("C");
        cafeAddress.setCity("D");
        cafeAddress.setProvince("E");
        return cafeAddress;
    }

    public Cafe newCafe() {
        Cafe cafe = new Cafe();
        cafe.setId(newCafeId());
        cafe.setCafeAddress(newCafeAddress());
        cafe.setCafeOfAllTimeRank(1);
        cafe.setCafeOfTheWeekWins(1);
        return cafe;
    }

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        cafeRecommendation = new CafeRecommendation(cafeDatabaseAdapter, twitter, dailyTweetRepo);
    }

    @AfterEach
    public void cleanUp() {
        cafeRecommendation.dailyCleanUp();
    }

    @Test
    public void testDailyTweetNoCafe() {
        doReturn(new Cafe[0]).when(cafeDatabaseAdapter).getAllCafe();
        cafeRecommendation.dailyTweet();
        verify(twitter, never()).updateStatus(any(String.class));
        verify(dailyTweetRepo, never()).saveAndFlush(any(DailyTweet.class));
    }

    @Test
    public void testDailyTweet1Cafe() {
        Cafe dummyCafe = newCafe();
        Cafe[] cafeOnDatabase = new Cafe[1];
        cafeOnDatabase[0] = dummyCafe;
        doReturn(cafeOnDatabase).when(cafeDatabaseAdapter).getAllCafe();
        Status status = new Status() {
            @Override
            public Date getCreatedAt() {
                return null;
            }

            @Override
            public long getId() {
                return 0;
            }

            @Override
            public String getText() {
                return null;
            }

            @Override
            public int getDisplayTextRangeStart() {
                return 0;
            }

            @Override
            public int getDisplayTextRangeEnd() {
                return 0;
            }

            @Override
            public String getSource() {
                return null;
            }

            @Override
            public boolean isTruncated() {
                return false;
            }

            @Override
            public long getInReplyToStatusId() {
                return 0;
            }

            @Override
            public long getInReplyToUserId() {
                return 0;
            }

            @Override
            public String getInReplyToScreenName() {
                return null;
            }

            @Override
            public GeoLocation getGeoLocation() {
                return null;
            }

            @Override
            public Place getPlace() {
                return null;
            }

            @Override
            public boolean isFavorited() {
                return false;
            }

            @Override
            public boolean isRetweeted() {
                return false;
            }

            @Override
            public int getFavoriteCount() {
                return 0;
            }

            @Override
            public User getUser() {
                return null;
            }

            @Override
            public boolean isRetweet() {
                return false;
            }

            @Override
            public Status getRetweetedStatus() {
                return null;
            }

            @Override
            public long[] getContributors() {
                return new long[0];
            }

            @Override
            public int getRetweetCount() {
                return 0;
            }

            @Override
            public boolean isRetweetedByMe() {
                return false;
            }

            @Override
            public long getCurrentUserRetweetId() {
                return 0;
            }

            @Override
            public boolean isPossiblySensitive() {
                return false;
            }

            @Override
            public String getLang() {
                return null;
            }

            @Override
            public Scopes getScopes() {
                return null;
            }

            @Override
            public String[] getWithheldInCountries() {
                return new String[0];
            }

            @Override
            public long getQuotedStatusId() {
                return 0;
            }

            @Override
            public Status getQuotedStatus() {
                return null;
            }

            @Override
            public URLEntity getQuotedStatusPermalink() {
                return null;
            }

            @Override
            public int compareTo(Status o) {
                return 0;
            }

            @Override
            public UserMentionEntity[] getUserMentionEntities() {
                return new UserMentionEntity[0];
            }

            @Override
            public URLEntity[] getURLEntities() {
                return new URLEntity[0];
            }

            @Override
            public HashtagEntity[] getHashtagEntities() {
                return new HashtagEntity[0];
            }

            @Override
            public MediaEntity[] getMediaEntities() {
                return new MediaEntity[0];
            }

            @Override
            public SymbolEntity[] getSymbolEntities() {
                return new SymbolEntity[0];
            }

            @Override
            public RateLimitStatus getRateLimitStatus() {
                return null;
            }

            @Override
            public int getAccessLevel() {
                return 0;
            }
        };
        doReturn(status).when(twitter).updateStatus(any(String.class));
        cafeRecommendation.dailyTweet();
        verify(twitter).updateStatus(any(String.class));
        verify(dailyTweetRepo).saveAndFlush(any(DailyTweet.class));
    }

    @Test
    public void testDailyTweet2AndMoreCafe() {
        Cafe dummyCafe = newCafe();
        Cafe[] cafeOnDatabase = new Cafe[3];
        cafeOnDatabase[0] = dummyCafe;

        dummyCafe = newCafe();
        CafeId dummy2Id = newCafeId();
        dummy2Id.setCafeOwnerTwitterId(101);
        dummy2Id.setCafeName("LOL");
        dummyCafe.setId(dummy2Id);
        dummyCafe.setCafeOfAllTimeRank(2);
        dummyCafe.setCafeOfTheWeekWins(0);
        cafeOnDatabase[1] = dummyCafe;

        dummyCafe = newCafe();
        CafeId dummy3Id = newCafeId();
        dummy3Id.setCafeOwnerTwitterId(9999);
        dummy3Id.setCafeName("AHO");
        dummyCafe.setId(dummy3Id);
        dummyCafe.setCafeOfAllTimeRank(2);
        dummyCafe.setCafeOfTheWeekWins(0);
        cafeOnDatabase[2] = dummyCafe;

        doReturn(cafeOnDatabase).when(cafeDatabaseAdapter).getAllCafe();
        Status status = new Status() {
            @Override
            public Date getCreatedAt() {
                return null;
            }

            @Override
            public long getId() {
                return 0;
            }

            @Override
            public String getText() {
                return null;
            }

            @Override
            public int getDisplayTextRangeStart() {
                return 0;
            }

            @Override
            public int getDisplayTextRangeEnd() {
                return 0;
            }

            @Override
            public String getSource() {
                return null;
            }

            @Override
            public boolean isTruncated() {
                return false;
            }

            @Override
            public long getInReplyToStatusId() {
                return 0;
            }

            @Override
            public long getInReplyToUserId() {
                return 0;
            }

            @Override
            public String getInReplyToScreenName() {
                return null;
            }

            @Override
            public GeoLocation getGeoLocation() {
                return null;
            }

            @Override
            public Place getPlace() {
                return null;
            }

            @Override
            public boolean isFavorited() {
                return false;
            }

            @Override
            public boolean isRetweeted() {
                return false;
            }

            @Override
            public int getFavoriteCount() {
                return 0;
            }

            @Override
            public User getUser() {
                return null;
            }

            @Override
            public boolean isRetweet() {
                return false;
            }

            @Override
            public Status getRetweetedStatus() {
                return null;
            }

            @Override
            public long[] getContributors() {
                return new long[0];
            }

            @Override
            public int getRetweetCount() {
                return 0;
            }

            @Override
            public boolean isRetweetedByMe() {
                return false;
            }

            @Override
            public long getCurrentUserRetweetId() {
                return 0;
            }

            @Override
            public boolean isPossiblySensitive() {
                return false;
            }

            @Override
            public String getLang() {
                return null;
            }

            @Override
            public Scopes getScopes() {
                return null;
            }

            @Override
            public String[] getWithheldInCountries() {
                return new String[0];
            }

            @Override
            public long getQuotedStatusId() {
                return 0;
            }

            @Override
            public Status getQuotedStatus() {
                return null;
            }

            @Override
            public URLEntity getQuotedStatusPermalink() {
                return null;
            }

            @Override
            public int compareTo(Status o) {
                return 0;
            }

            @Override
            public UserMentionEntity[] getUserMentionEntities() {
                return new UserMentionEntity[0];
            }

            @Override
            public URLEntity[] getURLEntities() {
                return new URLEntity[0];
            }

            @Override
            public HashtagEntity[] getHashtagEntities() {
                return new HashtagEntity[0];
            }

            @Override
            public MediaEntity[] getMediaEntities() {
                return new MediaEntity[0];
            }

            @Override
            public SymbolEntity[] getSymbolEntities() {
                return new SymbolEntity[0];
            }

            @Override
            public RateLimitStatus getRateLimitStatus() {
                return null;
            }

            @Override
            public int getAccessLevel() {
                return 0;
            }
        };
        doReturn(status).when(twitter).updateStatus(any(String.class));
        cafeRecommendation.dailyTweet();
        cafeRecommendation.dailyTweet();
        cafeRecommendation.dailyTweet();
        cafeRecommendation.dailyTweet();
        verify(twitter, atMost(3)).updateStatus(any(String.class));
        verify(dailyTweetRepo, atMost(3)).saveAndFlush(any(DailyTweet.class));
    }

    @Test
    public void testRankUpdate() {
        CafeId dummyCafeId1 = new CafeId();
        dummyCafeId1.setCafeOwnerTwitterId(123);
        dummyCafeId1.setCafeName("AHO");

        CafeId dummyCafeId2 = new CafeId();
        dummyCafeId2.setCafeOwnerTwitterId(456);
        dummyCafeId2.setCafeName("ABU");

        CafeId dummyCafeId3 = new CafeId();
        dummyCafeId3.setCafeOwnerTwitterId(789);
        dummyCafeId3.setCafeName("ANO");

        Cafe dummyA = newCafe();
        dummyA.setId(dummyCafeId1);
        dummyA.setCafeOfTheWeekWins(5);
        dummyA.setCafeOfAllTimeRank(2);

        Cafe dummyB = newCafe();
        dummyB.setId(dummyCafeId2);
        dummyB.setCafeOfTheWeekWins(2);
        dummyB.setCafeOfAllTimeRank(1);

        Cafe dummyC = newCafe();
        dummyC.setId(dummyCafeId3);
        dummyC.setCafeOfTheWeekWins(2);
        dummyC.setCafeOfAllTimeRank(3);

        Cafe[] cafeOnDatabase = {dummyA, dummyB, dummyC};
        ArgumentCaptor<List<Cafe>> argument = ArgumentCaptor.forClass(List.class);
        doReturn(cafeOnDatabase).when(cafeDatabaseAdapter).getAllCafe();
        doReturn(cafeOnDatabase).when(cafeDatabaseAdapter).deleteAllCafe();
        cafeRecommendation.rankUpdates();
        verify(cafeDatabaseAdapter).addMultipleCafe(argument.capture());
        List<Cafe> updatedRankCafe = argument.getValue();
        assertEquals(updatedRankCafe.get(0).getCafeOfAllTimeRank(), 1);
        assertTrue(updatedRankCafe.get(0).getId().equals(dummyCafeId1));
        assertEquals(updatedRankCafe.get(1).getCafeOfAllTimeRank(), 2);
        assertEquals(updatedRankCafe.get(2).getCafeOfAllTimeRank(), 3);
    }
}
