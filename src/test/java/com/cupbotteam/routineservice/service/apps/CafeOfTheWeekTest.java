package com.cupbotteam.routineservice.service.apps;

import com.cupbotteam.routineservice.core.Cafe;
import com.cupbotteam.routineservice.core.CafeAddress;
import com.cupbotteam.routineservice.core.CafeId;
import com.cupbotteam.routineservice.core.database.DailyTweet;
import com.cupbotteam.routineservice.repository.DailyTweetRepo;
import com.cupbotteam.routineservice.service.CafeDatabaseAdapter;
import com.cupbotteam.routineservice.service.Twitter4JAdapter;
import com.cupbotteam.routineservice.service.Twitter4JAdapterImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import twitter4j.Status;
import twitter4j.StatusUpdate;
import twitter4j.TwitterException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;

public class CafeOfTheWeekTest {
    
    Twitter4JAdapter twitter = new Twitter4JAdapterImpl();

    @Mock
    CafeDatabaseAdapter cafeDatabaseAdapter;

    @Mock
    DailyTweetRepo repo;

    @InjectMocks
    CafeOfTheWeek cotw;

    public DailyTweet newDailyTweet(long tweetId, String cafeName, long cafeOwnerTwitterId) {
        DailyTweet dailyTweet = new DailyTweet(tweetId, cafeName, cafeOwnerTwitterId);
        dailyTweet.setTweetDate(LocalDate.now(ZoneId.of("GMT+7")).minusDays(1));
        return dailyTweet;
    }

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        cotw = new CafeOfTheWeek(twitter, cafeDatabaseAdapter, repo);
    }

    @Test
    public void cafeOfTheWeekTest() throws TwitterException, InterruptedException {
        Status status1 = twitter.updateStatus("[DAILY RECOMMENDS]\nNama Cafe: CafeA\nAlamat: Jl. A");
        Status status2 = twitter.updateStatus("[DAILY RECOMMENDS]\nNama Cafe: CafeB\nAlamat: Jl. B");
        Status status3 = twitter.updateStatus("[DAILY RECOMMENDS]\nNama Cafe: CafeC\nAlamat: Jl. C");

        DailyTweet tweet1 = newDailyTweet(status1.getId(), "CafeA", 1268192067907227648L);
        DailyTweet tweet2 = newDailyTweet(status2.getId(), "CafeB", 1268192067907227648L);
        DailyTweet tweet3 = newDailyTweet(status3.getId(), "CafeC", 1268192067907227648L);

        twitter.createFavorite(status3.getId());
        Thread.sleep(30000);

        List<DailyTweet> tweetList = new ArrayList<>(List.of(tweet1, tweet2, tweet3));
        doReturn(tweetList).when(repo).findAllByTweetDate(any(LocalDate.class));

        cotw.selectCafeOfTheWeekNomination();
        assertEquals(cotw.getNominationsList().get(0),
                    tweet3.getCafeName() + " - " + tweet3.getCafeOwnerTwitterId());

        Status status4 = twitter.updateStatus("[DAILY RECOMMENDS]\nNama Cafe: CafeD\nAlamat: Jl. D");
        Status status5 = twitter.updateStatus("[DAILY RECOMMENDS]\nNama Cafe: CafeE\nAlamat: Jl. E");

        DailyTweet tweet4 = newDailyTweet(status4.getId(), "CafeD", 1268192067907227648L);
        DailyTweet tweet5 = newDailyTweet(status5.getId(), "CafeE", 1268192067907227648L);

        twitter.createFavorite(status4.getId());
        twitter.retweetStatus(status4.getId());
        twitter.retweetStatus(status5.getId());
        Thread.sleep(30000);

        tweetList = new ArrayList<>(List.of(tweet4, tweet5));
        doReturn(tweetList).when(repo).findAllByTweetDate(any(LocalDate.class));

        cotw.selectCafeOfTheWeekNomination();

        CafeId cafeId = new CafeId(1268192067907227648L, "CafeD");
        Cafe[] cafeArr = {new Cafe(cafeId, new CafeAddress("Jl. D", "", "", "", ""))};
        doReturn(cafeArr).when(cafeDatabaseAdapter).getCafe(any(CafeId.class));
        doReturn(cafeArr).when(cafeDatabaseAdapter).incrementCotwWins(any(CafeId.class));

        cotw.postCafeOfTheWeekNominations();
        assertEquals(cotw.getNominationsTweet().getText(),
                    "[NOMINASI CAFE OF THE WEEK]\nDukung cafe favoritmu dengan "
                    + "cara me-reply tweet ini dengan nomor urut cafe pilihanmu!\n\n1. "
                    + tweet3.getCafeName() + "\n2. "
                    + tweet4.getCafeName());
        Thread.sleep(30000);

        StatusUpdate update1 = new StatusUpdate("2");
        update1.setInReplyToStatusId(cotw.getNominationsTweet().getId());
        Status reply1 = twitter.updateStatus(update1);

        StatusUpdate update2 = new StatusUpdate("Invalid Vote");
        update2.setInReplyToStatusId(cotw.getNominationsTweet().getId());
        Status reply2 = twitter.updateStatus(update2);
        Thread.sleep(30000);

        cotw.setCafeOfTheWeekWinner();
        assertThat(cotw.getCafeOfTheWeekTweet().getText(),
                containsString("[PEMENANG CAFE OF THE WEEK]\n\nSelamat kepada pemenang minggu ini!\n"
                        + "Nama Cafe : " + tweet4.getCafeName()));

        twitter.destroyStatus(status1.getId());
        twitter.destroyStatus(status2.getId());
        twitter.destroyStatus(status3.getId());
        twitter.destroyStatus(status4.getId());
        twitter.destroyStatus(status5.getId());
        twitter.destroyStatus(reply1.getId());
        twitter.destroyStatus(reply2.getId());
        twitter.destroyStatus(cotw.getCafeOfTheWeekTweet().getId());
    }
}
